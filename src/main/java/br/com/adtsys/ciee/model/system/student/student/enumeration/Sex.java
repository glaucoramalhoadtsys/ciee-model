package br.com.adtsys.ciee.model.system.student.student.enumeration;

public enum Sex {

    FEMALE("FEMININO"),
    MALE("MASCULINO");

    private String translate;

    Sex(String translate) {

        this.translate = translate;
    }

    public String getTranslate() {
        return translate;
    }
}

package br.com.adtsys.ciee.model.system.core.user.enumerator;

public enum UserTypeEnum {

    BACKOFFICE("BACKOFFICE"),
    ESCOLA("ESCOLA");

    private String value;

    private UserTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}

package br.com.adtsys.ciee.model.system.unit.campus.model;

import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "campus_cursos")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CampusCourse {

    private static final long serialVersionUID = -9119414667915871292L;

    @Id
    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Size(message = "'Código Autorização' deve ter tamanho máximo de 20 caracteres", max = 20)
    @Column(name = "codigo_autorizacao", nullable = false, length = 20)
    private String authorizationCode;

    @NotNull(message = "'Termos aceito' deve ser preenchido")
    @Column(name = "aceite_termos", nullable = false)
    private Boolean acceptedTerms;

    @Builder.Default
    @Column(name = "bloqueado")
    private Boolean blocked = false;

    @NotNull(message = "'Curso' deve ser preenchido")
    @Column(name = "id_curso", nullable = false)
    private Long courseId;

    @Transient
    @JsonInclude(content = Include.NON_EMPTY)
    private String courseName;

    @ManyToOne
    @JoinColumn(name = "id_campus")
    @JsonBackReference
    private Campus campus;

    @OneToMany(mappedBy = "campusCourse")
    @JsonManagedReference
    private List<CampusCoursePeriod> coursePeriods;

    @ManyToOne(optional = true, cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "id_responsavel")
    private Person responsible;

    @Column(name = "codigo_ciee", nullable = true)
    @JsonIgnore
    private Long cieeCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public Boolean getAcceptedTerms() {
        return acceptedTerms;
    }

    public void setAcceptedTerms(Boolean acceptedTerms) {
        this.acceptedTerms = acceptedTerms;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Campus getCampus() {
        return campus;
    }

    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    public List<CampusCoursePeriod> getCoursePeriods() {
        return coursePeriods;
    }

    public void setCoursePeriods(List<CampusCoursePeriod> coursePeriods) {
        this.coursePeriods = coursePeriods;
    }

    public Person getResponsible() {
        return responsible;
    }

    public void setResponsible(Person responsible) {
        this.responsible = responsible;
    }

    public Long getCieeCode() {
        return cieeCode;
    }

    public void setCieeCode(Long cieeCode) {
        this.cieeCode = cieeCode;
    }
}

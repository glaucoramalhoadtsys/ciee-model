package br.com.adtsys.ciee.model.system.unit.zipcode.exception;

public class ZipcodeNotFound extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -5662779307345387232L;

    public ZipcodeNotFound(String msg) {
        super(msg);
    }
}

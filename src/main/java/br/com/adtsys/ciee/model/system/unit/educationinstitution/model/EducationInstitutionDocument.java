package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "instituicoes_ensinos_docs")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EducationInstitutionDocument implements Serializable {

    private static final long serialVersionUID = -5524093173520541482L;

    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    @JsonIgnore
    private Long id;

    @NotNull(message = "'Documento' deve ser preenchido")
    @Column(name = "id_documento", nullable = false, unique = true)
    private Long idDocument;


    @Column(name = "tipo_documento", nullable = false)
    private String type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EducationInstitutionDocument that = (EducationInstitutionDocument) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

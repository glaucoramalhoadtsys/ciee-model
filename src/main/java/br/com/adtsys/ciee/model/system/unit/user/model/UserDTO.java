package br.com.adtsys.ciee.model.system.unit.user.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Builder
@Data
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 6817225620457092931L;

    private String name;
    private String email;
    private String code;
    private String password;
    private String userType;
    private String roleName;
    private List<Long> domainIds;

}

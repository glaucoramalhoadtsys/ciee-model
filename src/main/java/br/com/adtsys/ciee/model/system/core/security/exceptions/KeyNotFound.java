package br.com.adtsys.ciee.model.system.core.security.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by adtsys on 6/8/17.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Chave JKS não foi encontrada.")
public class KeyNotFound extends RuntimeException {
    private static final long serialVersionUID = -2979765554620376302L;
}

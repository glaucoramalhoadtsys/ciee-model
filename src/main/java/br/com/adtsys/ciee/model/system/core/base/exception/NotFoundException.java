package br.com.adtsys.ciee.model.system.core.base.exception;

public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = -6124289134512655064L;

    public NotFoundException(String msg) {
        super(msg);
    }

    public NotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }
}

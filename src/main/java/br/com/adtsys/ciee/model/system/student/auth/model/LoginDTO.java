package br.com.adtsys.ciee.model.system.student.auth.model;

import br.com.adtsys.ciee.model.system.student.auth.enumeration.LoginTypeEnum;

public class LoginDTO {
    private Long id;
    private String userLogin;
    private LoginTypeEnum type;
}

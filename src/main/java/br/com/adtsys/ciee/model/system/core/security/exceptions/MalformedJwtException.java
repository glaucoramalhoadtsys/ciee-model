package br.com.adtsys.ciee.model.system.core.security.exceptions;


public class MalformedJwtException extends RuntimeException {
    public MalformedJwtException(final String message) {
        super(message);
    }
}

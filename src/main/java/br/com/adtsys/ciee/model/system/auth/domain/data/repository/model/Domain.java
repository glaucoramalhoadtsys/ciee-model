package br.com.adtsys.ciee.model.system.auth.domain.data.repository.model;

import br.com.adtsys.ciee.model.system.auth.base.AbstractEntity;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.util.Collection;

/**
 * AbstractEntity Domain. AbstractEntity of the User's domain.
 *
 * @author Maiara Rodrigues - maiara.rodrigues@adtsys.com.br
 * @version 1.0, Jun 2017
 */
@Builder
@Entity(name = "dominios")
public class Domain extends AbstractEntity {

    private static final long serialVersionUID = 8623165847991219766L;
    @OneToMany(mappedBy = "parentDomain")
    private Collection<Domain> childDomain;
    @Id
    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_dominio_pai")
    private Domain parentDomain;
    @Column(name = "descricao")
    private String description;

    @Tolerate
    public Domain() {
    }

    public Collection<Domain> getChildDomain() {
        return this.childDomain;
    }

    public void setChildDomain(final Collection<Domain> childDomain) {
        this.childDomain = childDomain;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Domain getParentDomain() {
        return this.parentDomain;
    }

    public void setParentDomain(final Domain parentDomain) {
        this.parentDomain = parentDomain;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        final Domain domain = (Domain) o;

        return this.id.equals(domain.id);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + this.id.hashCode();
        return result;
    }
}

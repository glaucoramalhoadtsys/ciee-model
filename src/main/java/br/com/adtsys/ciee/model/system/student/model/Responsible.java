package br.com.adtsys.ciee.model.system.student.model;

public class Responsible {


    private Long id;

    private String motherName;

    private String fatherName;

    private String legalGuardianName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

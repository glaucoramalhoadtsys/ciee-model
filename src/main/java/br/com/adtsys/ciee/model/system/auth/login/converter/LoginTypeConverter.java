package br.com.adtsys.ciee.model.system.auth.login.converter;

import br.com.adtsys.ciee.model.system.auth.login.enumeration.LoginTypeEnum;

import javax.persistence.AttributeConverter;
import java.util.Objects;

public class LoginTypeConverter implements AttributeConverter<LoginTypeEnum, String> {

    @Override
    public String convertToDatabaseColumn(LoginTypeEnum loginType) {
        return Objects.nonNull(loginType) ? loginType.getType() : null;
    }

    @Override
    public LoginTypeEnum convertToEntityAttribute(String string) {
        for (final LoginTypeEnum type : LoginTypeEnum.values()) {
            if (string.equals((type.getType()))) {
                return type;
            }
        }
        return null;
    }
}

package br.com.adtsys.ciee.model.system.auth.roles.exception;

public class RoleNotFound extends RuntimeException {
    private static final long serialVersionUID = -578725186234005872L;

    public RoleNotFound(String msg) {
        super(msg);
    }
}

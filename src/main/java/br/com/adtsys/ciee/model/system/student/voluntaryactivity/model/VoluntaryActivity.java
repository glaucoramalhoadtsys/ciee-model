package br.com.adtsys.ciee.model.system.student.voluntaryactivity.model;

import br.com.adtsys.ciee.model.system.student.student.model.StudentResource;

/**
 * Entidade que representa um Trabalho Voluntário.
 */

public class VoluntaryActivity extends StudentResource {


    private Long id;

    private String institutionName;

    private String period;

    private String description;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

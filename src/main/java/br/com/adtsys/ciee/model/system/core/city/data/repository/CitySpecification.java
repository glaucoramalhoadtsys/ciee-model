package br.com.adtsys.ciee.model.system.core.city.data.repository;

import br.com.adtsys.ciee.model.system.core.city.data.repository.model.City;
import br.com.adtsys.ciee.model.system.core.sql.ObjectsEnumerator;
import br.com.adtsys.ciee.model.system.core.state.repository.model.State;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class CitySpecification implements Specification<City> {

    private static final String FUNCTION_STRIP_ACCENTS = ObjectsEnumerator.FUNCTION_STRIP_ACCENTS.getName();

    private String filter;

    public CitySpecification(String filter) {
        this.filter = filter;
    }

    private void add(Predicate predicate, Expression<Boolean> expression) {
        predicate.getExpressions().add(expression);
    }

    @Override
    public Predicate toPredicate(Root<City> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        Predicate predicate = criteriaBuilder.conjunction();

        Join<City, State> stateJoin = root.join("state");

        if (StringUtils.isNotBlank(this.filter)) {

            String normalizedFilter = StringUtils.stripAccents(filter).toUpperCase();

            add(predicate,
                    criteriaBuilder.or(
                            criteriaBuilder.like(
                                    criteriaBuilder.function(FUNCTION_STRIP_ACCENTS, String.class,
                                            criteriaBuilder.upper(root.get("cityName"))), "%" + normalizedFilter + "%"),
                            criteriaBuilder.like(
                                    criteriaBuilder.function(FUNCTION_STRIP_ACCENTS, String.class,
                                            criteriaBuilder.upper(stateJoin.get("initials"))), "%" + normalizedFilter + "%")));
        }

        add(predicate, criteriaBuilder.and(
                criteriaBuilder.equal(root.get("deleted"), false)));

        add(predicate, criteriaBuilder.and(
                criteriaBuilder.equal(root.get("active"), true)));

        return predicate;
    }

}

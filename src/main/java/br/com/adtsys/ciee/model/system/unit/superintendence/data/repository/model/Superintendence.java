package br.com.adtsys.ciee.model.system.unit.superintendence.data.repository.model;

import br.com.adtsys.ciee.model.system.unit.unit.data.repository.model.Unit;
import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;

public class Superintendence {

  public Superintendence() {
  }

  private Long id;

  private String description;

  private String initials;

  private String shortDescription;

  private Person responsibleContact;

  private Unit unit;

  private Boolean active;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getInitials() {
    return initials;
  }

  public void setInitials(String initials) {
    this.initials = initials;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public Person getResponsibleContact() {
    return responsibleContact;
  }

  public void setResponsibleContact(Person responsibleContact) {
    this.responsibleContact = responsibleContact;
  }

  public Unit getUnit() {
    return unit;
  }

  public void setUnit(Unit unit) {
    this.unit = unit;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }
}

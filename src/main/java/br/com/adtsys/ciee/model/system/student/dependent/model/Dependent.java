package br.com.adtsys.ciee.model.system.student.dependent.model;

import br.com.adtsys.ciee.model.system.student.dependenttype.model.DependentType;
import br.com.adtsys.ciee.model.system.student.student.model.StudentResource;

import java.time.LocalDate;

/**
 * Entidade que representa um Dependente.
 */

public class Dependent extends StudentResource {


    private Long id;

    private DependentType dependentType;

    private String name;

    private LocalDate birthDate;

    private String cpf;

    private Boolean incomeTax;

    private Boolean workCapability;

    private Boolean familySalary;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

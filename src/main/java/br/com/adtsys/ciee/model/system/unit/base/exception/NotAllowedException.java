package br.com.adtsys.ciee.model.system.unit.base.exception;

public class NotAllowedException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -8547032441650659485L;

    public NotAllowedException(String msg) {
        super(msg);
    }
}

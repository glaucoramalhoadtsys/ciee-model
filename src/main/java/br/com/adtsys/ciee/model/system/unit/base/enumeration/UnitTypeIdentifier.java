package br.com.adtsys.ciee.model.system.unit.base.enumeration;

public enum UnitTypeIdentifier {
    CIEE, SUPERINTENDENCE, MANAGEMENT, CIEE_UNIT, TRAINING_CENTER, SERVICE_STATION
}


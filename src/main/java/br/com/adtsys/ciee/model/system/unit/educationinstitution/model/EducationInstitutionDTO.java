package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import br.com.adtsys.ciee.model.system.unit.address.data.repository.model.Address;
import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class EducationInstitutionDTO {
    private Long id;

    private String preRegistrationSituation;

    private String institutionStatus;

    private String institutionName;

    private String educationDegree;

    private String course;

    private String cnpj;

    private String authorizationCode;

    private Long institutionType;

    private Boolean active;

    private Address address;

    private String protocolNumber;

    private String password;

    private List<Person> contacts;

    private List<EducationInstitutionDocument> documents;

    private ZonedDateTime modifiedDate;

    @Builder.Default
    private Boolean ead = false;

    public EducationInstitutionDTO() {
        this.documents = new ArrayList<>();
    }

}

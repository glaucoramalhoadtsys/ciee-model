package br.com.adtsys.ciee.model.system.student.student.enumeration;

import br.com.adtsys.ciee.model.system.student.student.validator.StudentValidator;
import br.com.adtsys.ciee.model.system.student.student.validator.impl.PCDAnalyzedStatusValidator;

import java.util.Optional;

/**
 * Contém as possíveis situações que uma análise PCD de estudante pode passar
 * durante o processo realizado pelo backoffice.
 */
public enum PCDAnalysisStatusEnum {

    PENDING("PENDENTE", Optional.empty()),
    ANALYZING("ANALISANDO", Optional.empty()),
    ANALYZED("ANALISADO", Optional.ofNullable(new PCDAnalyzedStatusValidator()));

    private String status;

    private Optional<StudentValidator> validator;

    private PCDAnalysisStatusEnum(String status, Optional<StudentValidator> validator) {
        this.status = status;
        this.validator = validator;
    }

    public String getStatus() {
        return status;
    }

    public Optional<StudentValidator> getValidator() {
        return validator;
    }

}

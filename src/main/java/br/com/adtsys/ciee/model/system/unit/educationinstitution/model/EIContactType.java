package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import br.com.adtsys.ciee.model.system.unit.processtype.model.ProcessType;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "contato_ie_tipo_processo")
@Data
@EqualsAndHashCode(exclude = "educationInstitution")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EIContactType implements Serializable {

    private static final long serialVersionUID = 2774667220954700790L;

    @Id
    @ManyToOne
    @JoinColumn(name = "id_instituicao_ensino")
    private EducationInstitution educationInstitution;

    @Id
    @ManyToOne
    @JoinColumn(name = "id_canal_comunicacao")
    private ProcessType processType;

    @Id
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "id_pessoa")
    private Person person;

    @Column
    @Builder.Default
    private Boolean active = true;

    @Column
    @Builder.Default
    private Boolean mainContact = false;

    @Column
    @Builder.Default
    private Boolean director = false;

    @Column
    @Builder.Default
    private Boolean viceDirector = false;
}

package br.com.adtsys.ciee.model.system.unit.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {

    private static final long serialVersionUID = -6064241148535833862L;

    public ConflictException() {
        this("Foi encontrado um conflito de informações na base!");
    }

    public ConflictException(String msg) {
        super(msg);
    }
}

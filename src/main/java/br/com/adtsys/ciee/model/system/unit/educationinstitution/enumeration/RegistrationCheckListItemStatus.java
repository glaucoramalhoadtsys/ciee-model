package br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration;

import com.fasterxml.jackson.annotation.JsonValue;

public enum RegistrationCheckListItemStatus {
    PENDING("PENDENTE"),
    APPROVED("APROVADO"),
    DISAPPROVED("REPROVADO");

    private String value;

    private RegistrationCheckListItemStatus(String value) {
        this.value = value;
    }

    public static RegistrationCheckListItemStatus findByValue(String value) {
        for (RegistrationCheckListItemStatus v : values()) {
            if (v.getValue().equals(value)) {
                return v;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}

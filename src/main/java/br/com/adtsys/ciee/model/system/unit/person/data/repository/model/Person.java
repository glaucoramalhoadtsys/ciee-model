package br.com.adtsys.ciee.model.system.unit.person.data.repository.model;

import br.com.adtsys.ciee.model.system.unit.email.data.repository.model.Email;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.model.EIContactType;
import br.com.adtsys.ciee.model.system.unit.phone.data.repository.model.Phone;

import java.util.Set;

public class Person {

    private Long id;
    private String name;
    private String shortname;
    private String role;
    private String shortRole;
    private String department;
    private String registration;
    private String documentationNumber;
    private Long cieeCode;
    private Set<Email> emails;
    private Set<Phone> phones;
    private Set<EIContactType> eiContactTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getShortRole() {
        return shortRole;
    }

    public void setShortRole(String shortRole) {
        this.shortRole = shortRole;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getDocumentationNumber() {
        return documentationNumber;
    }

    public void setDocumentationNumber(String documentationNumber) {
        this.documentationNumber = documentationNumber;
    }

    public Long getCieeCode() {
        return cieeCode;
    }

    public void setCieeCode(Long cieeCode) {
        this.cieeCode = cieeCode;
    }

    public Set<Email> getEmails() {
        return emails;
    }

    public void setEmails(Set<Email> emails) {
        this.emails = emails;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    public Set<EIContactType> getEiContactTypes() {
        return eiContactTypes;
    }

    public void setEiContactTypes(Set<EIContactType> eiContactTypes) {
        this.eiContactTypes = eiContactTypes;
    }
}

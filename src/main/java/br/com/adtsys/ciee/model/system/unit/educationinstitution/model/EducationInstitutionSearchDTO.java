package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EducationInstitutionSearchDTO {
    private List<Long> domainIds;
    private Long id;
    private String institutionName;
    private List<String> cnpjs;
    private String cnpj;
    private String protocolNumber;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startAt;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate finishAt;
    private String eiSituation;
    private String addressState;
}

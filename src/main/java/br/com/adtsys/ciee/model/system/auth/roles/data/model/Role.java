package br.com.adtsys.ciee.model.system.auth.roles.data.model;

import br.com.adtsys.ciee.model.system.auth.permission.data.repository.model.Permission;

import java.util.Set;

public class Role  {

    public static final String STUDENT_DESCRIPTION = "Estudante";
    private static final long serialVersionUID = 6756299927322603520L;
    private Long id;


    private String description;

    private Set<Permission> permissions;

    public static String getStudentDescription() {
        return STUDENT_DESCRIPTION;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }
}

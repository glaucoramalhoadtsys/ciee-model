package br.com.adtsys.ciee.model.system.core.gender.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * POJO filtro para busca de gêneros
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenderFilterDTO {

    private Long id;

    private String description;

    private Boolean active;

}

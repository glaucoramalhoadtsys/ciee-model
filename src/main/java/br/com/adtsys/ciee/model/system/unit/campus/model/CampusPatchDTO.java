package br.com.adtsys.ciee.model.system.unit.campus.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CampusPatchDTO {
    private String name;
    private String fantasyName;
    private String cnpj;
    private Long walletId;
    private Boolean pronatec;
    private Boolean active;
}

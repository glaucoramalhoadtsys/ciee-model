package br.com.adtsys.ciee.model.system.core.address.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Entidade para persistência de Endereços
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "ENDERECOS")
public class Address extends AbstractEntity {

    private static final long serialVersionUID = -7874949746060639137L;

    @Id
    @GeneratedValue(generator = "custom-sequence", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Size(message = "'Endereço' deve ter tamanho máximo de 150 caracteres", max = 150)
    @Column(name = "ENDERECO", length = 150, nullable = false)

    private String address;

    @Size(message = "'Tipo' deve ter tamanho máximo de 40 caracteres", max = 40)
    @Column(name = "TIPO", length = 40)
    private String type;

    @Size(message = "'Número' deve ter tamanho máximo de 10 caracteres", max = 10)
    @Column(name = "NUMERO", length = 10)
    private String number;

    @Size(message = "'Complemento' deve ter tamanho máximo de 50 caracteres", max = 50)
    @Column(name = "COMPLEMENTO", length = 50)
    private String complement;

    @Size(message = "'Bairro' deve ter tamanho máximo de 100 caracteres", max = 100)
    @Column(name = "BAIRRO", length = 100)
    private String neighborhood;


    @Size(message = "'CEP' deve ter tamanho máximo de 8 caracteres", max = 8)
    @Column(name = "CEP", nullable = false, length = 8)
    private String zipCode;

    @Size(message = "'Cidade' deve ter tamanho máximo de 100 caracteres", max = 100)
    @Column(name = "CIDADE", length = 100)
    private String city;

    @Size(message = "'UF' deve ter tamanho de 2 caracteres", max = 2)
    @Column(name = "UF", length = 2)
    private String state;
}
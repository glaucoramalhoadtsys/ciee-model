package br.com.adtsys.ciee.model.system.student.student.enumeration;

public enum PCDDisabilityTypeEnum {

    PARTIAL("PARCIAL"),
    TOTAL("TOTAL");

    private String type;

    private PCDDisabilityTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}

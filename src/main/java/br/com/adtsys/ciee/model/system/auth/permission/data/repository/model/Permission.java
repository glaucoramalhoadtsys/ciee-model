package br.com.adtsys.ciee.model.system.auth.permission.data.repository.model;

import br.com.adtsys.ciee.model.system.auth.base.AbstractEntity;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Permission User system. AbstractEntity of the User's permissions.
 *
 * @author Maiara Rodrigues - maiara.rodrigues@adtsys.com.br
 * @version 1.0, Jun 2017
 */
@Entity(name = "permissoes")
public class Permission extends AbstractEntity {

    private static final long serialVersionUID = 3687486059727061728L;
    /**
     * Permission Id.
     */
    @Id
    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    /**
     * Abbreviation of Permission.
     */

    @Column(name = "sigla_permissao", nullable = false)
    @Size(max = 15, message = "'Sigla da Permissão' deve ter no máximo 15 caracteres")
    private String permissionAbbreviation;
    /**
     * Description of Permission.
     */

    @Column(name = "desc_permissao", nullable = false)
    @Size(max = 30, message = "'Descrição' deve ter no máximo 30 caracteres")
    private String permissionDescription;
    /**
     * Permission join column - necessary for the self-relationship.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_permissao", insertable = true, updatable = true, nullable = true)
    private Permission parentPermission;

    @Tolerate
    public Permission() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getPermissionAbbreviation() {
        return this.permissionAbbreviation;
    }

    public void setPermissionAbbreviation(final String permissionAbbreviation) {
        this.permissionAbbreviation = permissionAbbreviation;
    }

    public String getPermissionDescription() {
        return this.permissionDescription;
    }

    public void setPermissionDescription(final String permissionDescription) {
        this.permissionDescription = permissionDescription;
    }

    public Permission getParentPermission() {
        return this.parentPermission;
    }

    public void setParentPermission(final Permission parentPermission) {
        this.parentPermission = parentPermission;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Permission)) return false;

        final Permission that = (Permission) o;

        return getId() != null ? getId().equals(that.getId()) : that.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}

package br.com.adtsys.ciee.model.system.auth.authentication.model;

/**
 * Enum de tipos de Usuários
 */
public enum UserTypeEnum {
    BACKOFFICE("BACKOFFICE"),
    SCHOOL("ESCOLA"),
    STUDENT("ESTUDANTE");

    private String value;

    private UserTypeEnum(String value) {
        this.value = value;
    }

    public static UserTypeEnum findByValue(String value) {
        for (final UserTypeEnum userTypeEnum : values()) {
            if (value.equalsIgnoreCase(userTypeEnum.getValue())) {
                return userTypeEnum;
            }
        }

        return null;
    }

    public String getValue() {
        return value;
    }
}

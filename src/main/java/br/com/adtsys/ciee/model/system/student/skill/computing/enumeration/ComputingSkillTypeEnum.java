package br.com.adtsys.ciee.model.system.student.skill.computing.enumeration;

/**
 * Tipos de conhecimentos em informática
 */
public enum ComputingSkillTypeEnum {

    TEXT("TEXTO"),
    SPREADSHEET("PLANILHA"),
    PRESENTATION("APRESENTACAO");

    private String type;

    private ComputingSkillTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}

package br.com.adtsys.ciee.model.system.unit.base;

import br.com.adtsys.ciee.model.system.unit.base.exception.PDFBaseFontLoadException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;

import java.io.IOException;
import java.net.URL;

public class CieeBasePDF {
    public static BaseFont ARIAL_FONT = getBaseFont("fonts/arial.ttf");
    public static BaseFont ARIAL_BOLD_FONT = getBaseFont("fonts/arial-bold.ttf");
    public static BaseFont ARIAL_ITALIC_FONT = getBaseFont("fonts/arial-italic.ttf");

    public static BaseFont getBaseFont(final String fontResourcePath) {
        try {
            URL imagePath = CieeBasePDF.class.getClassLoader().getResource(fontResourcePath);
            return BaseFont.createFont(imagePath.toString(), BaseFont.WINANSI, BaseFont.EMBEDDED);

        } catch (DocumentException | IOException e) {
            throw new PDFBaseFontLoadException("Erro ao carregar a font: " + fontResourcePath);
        }
    }
}

package br.com.adtsys.ciee.model.system.auth.authentication.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.CONFLICT;

@ResponseStatus(value = CONFLICT, reason = "Existe mais de um email cadastrado, utilize o CPF, código de estudante ou nome de usuário.")
public class NonUniqueUserException extends RuntimeException {

    private static final long serialVersionUID = 8794807321302632082L;

    public NonUniqueUserException() {
        super();
    }

    public NonUniqueUserException(final String message) {
        super(message);
    }

}

package br.com.adtsys.ciee.model.system.core.phone.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Entidade para persistência de Telefones
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "TELEFONES")
public class Phone extends AbstractEntity {

    private static final long serialVersionUID = -6955695071293094902L;

    @Id
    @GeneratedValue(generator = "custom-sequence", strategy = GenerationType.SEQUENCE)
    private Long id;


    @Size(message = "'DDD' deve ter tamanho de 2 caracteres", min = 2, max = 2)
    @Column(name = "DDD", nullable = false, length = 2)
    private String areaCode;


    @Size(message = "'Número' deve ter tamanho máximo de 9 caracteres", max = 9)
    @Column(name = "NUMERO", nullable = false, length = 9)
    private String number;

    @Size(message = "'Descrição' deve ter tamanho máximo de 100 caracteres", max = 100)
    @Column(name = "DESCRICAO", length = 100)
    private String description;

    @Size(message = "'Ramal' deve ter tamanho máximo de 9 caracteres", max = 9)
    @Column(name = "RAMAL", length = 9)
    private String extension;

    @Column(name = "TIPO_TELEFONE")
    private String phoneType;
}
package br.com.adtsys.ciee.model.system.student.language.model;

public enum Language {
    ENGLISH("INGLÊS"),
    GERMAN("ALEMÃO"),
    SPANISH("ESPANHOL"),
    ITALIAN("ITALIANO"),
    JAPANESE("JAPONÊS"),
    FRENCH("FRANCÊS");

    private String translate;

    private Language(String translate) {
        this.translate = translate;
    }

    public String getTranslate() {
        return this.translate;
    }
}

package br.com.adtsys.ciee.model.system.student.student.validator.impl;

import br.com.adtsys.ciee.model.system.student.student.model.Student;
import br.com.adtsys.ciee.model.system.student.student.validator.StudentValidator;


/**
 * Implementação de {@link StudentValidator} responsável por validar situação "ANALISADA" de análise PCD
 */
public class PCDAnalyzedStatusValidator implements StudentValidator {

    private static final String ERROR_MESSAGE = "Tipo de deficiência, válido para cota e usa recursos de acessibilidade devem ser preenchidos";

    private Boolean isInvalidState(Student student) {

        /*return Objects.isNull(student.getPcdDisabilityType()) ||
                Objects.isNull(student.getPcdEligibility()) ||
                Objects.isNull(student.getPcdUseAppliances());*/
        return false;
    }


    public void validate(Student student) throws Exception {

        if (isInvalidState(student)) {
            throw new Exception();
        }
    }

}

package br.com.adtsys.ciee.model.system.student.student.enumeration;

public enum Ages {
        ADULTA("18 years ago"),
        MAIOR_QUE_DEZESSEIS("16 years ago"),
        MENOR_QUE_DEZESSES("14 years ago");
        private String age;

        Ages(String age) {
            this.age = age;
        }

        public String getAge() {
            return age;
        }
    }
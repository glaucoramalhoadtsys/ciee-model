package br.com.adtsys.ciee.model.system.core.sql;

/**
 * Enum para declaração de objetos sql (procedures, functions) usados pela aplicação.
 */
public enum ObjectsEnumerator {

    FUNCTION_STRIP_ACCENTS("removeAcento");

    private String name;

    private ObjectsEnumerator(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}

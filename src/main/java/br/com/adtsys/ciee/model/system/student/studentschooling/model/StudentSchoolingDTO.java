package br.com.adtsys.ciee.model.system.student.studentschooling.model;


import br.com.adtsys.ciee.model.system.student.studentenem.model.StudentEnem;
import br.com.adtsys.ciee.model.system.unit.campus.model.CampusCoursePeriodDTO;

/**
 * DTO que contém dados detalhados de escolaridade de um estudante.
 */

public class StudentSchoolingDTO {

    private String studentCode;
    private StudentEnem enem;
    private StudentSchooling studentSchooling;
    /*private CampusDTO campus;
    private CampusCourseDTO campusCourse;
    private CourseDTO course;*/
    private CampusCoursePeriodDTO campusCoursePeriod;
}

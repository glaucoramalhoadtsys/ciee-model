package br.com.adtsys.ciee.model.system.student.student.model;

/**
 * Classe pai de um Recurso do Estudante.
 */

public abstract class StudentResource {

    private Student student;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}

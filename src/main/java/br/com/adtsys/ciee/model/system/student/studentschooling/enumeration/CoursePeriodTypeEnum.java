package br.com.adtsys.ciee.model.system.student.studentschooling.enumeration;

public enum CoursePeriodTypeEnum {

    M("Manhã"),
    T("Tarde"),
    N("Noite"),
    I("Integral"),
    E("Vespertino"),
    V("Variável");

    private String value;

    private CoursePeriodTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

package br.com.adtsys.ciee.model.system.unit.base.enumeration;

public enum EducationDegreeEnum {
    SU("Superior"),
    TE("Técnico"),
    EE("Educação Especial"),
    HB("Habilitação Básica"),
    EM("Ensino Médio"),
    EF("Ensino Fundamental");

    private String value;

    private EducationDegreeEnum(String value) {
        this.value = value;
    }

    public static EducationDegreeEnum findByKey(String key) {
        for (final EducationDegreeEnum degreeEnum : values()) {
            if (key.equals(degreeEnum.toString())) {
                return degreeEnum;
            }
        }

        return null;
    }

    public String getValue() {
        return this.value;
    }

}

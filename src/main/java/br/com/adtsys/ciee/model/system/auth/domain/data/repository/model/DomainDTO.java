package br.com.adtsys.ciee.model.system.auth.domain.data.repository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DomainDTO implements Serializable {
    private static final long serialVersionUID = -2693534584718104509L;

    private Long id;


    private Long parentDomainId;

    private String description;
}

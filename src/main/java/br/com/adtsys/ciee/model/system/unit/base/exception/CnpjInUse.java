package br.com.adtsys.ciee.model.system.unit.base.exception;

public class CnpjInUse extends RuntimeException {

    private static final long serialVersionUID = -805653825174905638L;

    public CnpjInUse() {
        super("CNPJ já está sendo utilizado!");
    }
}

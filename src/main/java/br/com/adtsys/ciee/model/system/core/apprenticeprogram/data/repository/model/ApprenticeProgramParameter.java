package br.com.adtsys.ciee.model.system.core.apprenticeprogram.data.repository.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Entity(name = "parametros_programa_aprendiz")
@AllArgsConstructor
public class ApprenticeProgramParameter extends AbstractEntity {

    private static final long serialVersionUID = -5514818307208921306L;
    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "codigo_ciee", unique = true)
    private Integer code;
    @Column(name = "idade_minima_ano", nullable = false)
    private Integer minimumAgeYear;
    @Column(name = "idade_minima_mes", nullable = false)
    private Integer minimumMonthYear;
    @Column(name = "idade_minima_dia", nullable = false)
    private Integer minimumDayYear;
    @Column(name = "idade_maxima_ano", nullable = false)
    private Integer maximumAgeYear;
    @Column(name = "idade_maxima_mes", nullable = false)
    private Integer maximumMonthYear;
    @Column(name = "idade_maxima_dia", nullable = false)
    private Integer maximumDayYear;
    @Column(name = "ci_minimo_capacitador", nullable = false, precision = 10, scale = 2)
    private BigDecimal minimumCIEnabler;
    @Column(name = "ci_desejavel_capacitador", nullable = false, precision = 10, scale = 2)
    private BigDecimal desirableCIEnabler;
    @Column(name = "ci_minimo_empregador", nullable = false, precision = 10, scale = 2)
    private BigDecimal minimumCIEmployer;
    @Column(name = "ci_desejavel_empregador", nullable = false, precision = 10, scale = 2)
    private BigDecimal desirableCIEmployer;
    @Column(name = "ci_minimo_legal_capacitador", nullable = false, precision = 10, scale = 2)
    private BigDecimal minimumLegalCIEnabler;
    @Column(name = "ci_desejavel_legal_capacitador", nullable = false, precision = 10, scale = 2)
    private BigDecimal desirableLegalCIEnabler;
    @Column(name = "ci_minimo_legal_empregador", nullable = false, precision = 10, scale = 2)
    private BigDecimal minimumLegalCIEmployer;
    @Column(name = "ci_desejavel_legal_empregador", nullable = false, precision = 10, scale = 2)
    private BigDecimal desirableLegalCIEmployer;
    @Column(name = "idade_limite_contratacao_ano", nullable = false)
    private Integer limitContractingAgeYear;
    @Column(name = "idade_limite_contratacao_mes", nullable = false)
    private Integer limitContractingAgeMonth;
    @Column(name = "idade_limite_contratacao_dia", nullable = false)
    private Integer limitContractingAgeDay;
    @Column(name = "jornada_dia_minima", nullable = false)
    private Short minimumDailyJourney;
    @Column(name = "jornada_dia_maxima_fundamental", nullable = false)
    private Short maximumDailyJourneyElementarySchool;
    @Column(name = "jornada_dia_maxima_medio", nullable = false)
    private Short maximumDailyJourneySecondarySchool;
    @Column(name = "minimo_dia_contrato", nullable = false)
    private Integer minimumContractDays;
    @Column(name = "quantidade_minima_atividades", nullable = false)
    private Integer minimumAmountActivities;
    @Column(name = "quantidade_maxima_atividades", nullable = false)
    private Integer maximumAmountActivities;
    @Column(name = "limite_cancelamento", nullable = false)
    private Integer cancelationDeadline;
    @Column(name = "limite_carta_rescisao", nullable = false)
    private Integer terminationLetterDeadline;
    @Column(name = "dias_antecipacao_rescisao", nullable = false)
    private Integer daysAnticipationTermination;
    @NotNull(message = "'Curso Ensino Fundamental' deve ser preechido")
    @Column(name = "curso_fundamental", nullable = false)
    private Boolean elementarySchoolCourse;
    @NotNull(message = "'Curso Ensino Médio' deve ser preechido")
    @Column(name = "curso_medio", nullable = false)
    private Boolean secondarySchoolCourse;
    @NotNull(message = "'Curso Superior' deve ser preechido")
    @Column(name = "curso_superior", nullable = false)
    private Boolean highEducationCourse;
    @NotNull(message = "'Curso Técnico' deve ser preechido")
    @Column(name = "curso_tecnico", nullable = false)
    private Boolean technicalCourse;
    @Column(name = "escola_fundamental_aprendiz", nullable = false)
    private Integer fundamentalApprenticeSchool;


    @Tolerate
    public ApprenticeProgramParameter() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getMinimumAgeYear() {
        return minimumAgeYear;
    }

    public void setMinimumAgeYear(Integer minimumAgeYear) {
        this.minimumAgeYear = minimumAgeYear;
    }

    public Integer getMinimumMonthYear() {
        return minimumMonthYear;
    }

    public void setMinimumMonthYear(Integer minimumMonthYear) {
        this.minimumMonthYear = minimumMonthYear;
    }

    public Integer getMinimumDayYear() {
        return minimumDayYear;
    }

    public void setMinimumDayYear(Integer minimumDayYear) {
        this.minimumDayYear = minimumDayYear;
    }

    public Integer getMaximumAgeYear() {
        return maximumAgeYear;
    }

    public void setMaximumAgeYear(Integer maximumAgeYear) {
        this.maximumAgeYear = maximumAgeYear;
    }

    public Integer getMaximumMonthYear() {
        return maximumMonthYear;
    }

    public void setMaximumMonthYear(Integer maximumMonthYear) {
        this.maximumMonthYear = maximumMonthYear;
    }

    public Integer getMaximumDayYear() {
        return maximumDayYear;
    }

    public void setMaximumDayYear(Integer maximumDayYear) {
        this.maximumDayYear = maximumDayYear;
    }

    public BigDecimal getMinimumCIEnabler() {
        return minimumCIEnabler;
    }

    public void setMinimumCIEnabler(BigDecimal minimumCIEnabler) {
        this.minimumCIEnabler = minimumCIEnabler;
    }

    public BigDecimal getDesirableCIEnabler() {
        return desirableCIEnabler;
    }

    public void setDesirableCIEnabler(BigDecimal desirableCIEnabler) {
        this.desirableCIEnabler = desirableCIEnabler;
    }

    public BigDecimal getMinimumCIEmployer() {
        return minimumCIEmployer;
    }

    public void setMinimumCIEmployer(BigDecimal minimumCIEmployer) {
        this.minimumCIEmployer = minimumCIEmployer;
    }

    public BigDecimal getDesirableCIEmployer() {
        return desirableCIEmployer;
    }

    public void setDesirableCIEmployer(BigDecimal desirableCIEmployer) {
        this.desirableCIEmployer = desirableCIEmployer;
    }

    public BigDecimal getMinimumLegalCIEnabler() {
        return minimumLegalCIEnabler;
    }

    public void setMinimumLegalCIEnabler(BigDecimal minimumLegalCIEnabler) {
        this.minimumLegalCIEnabler = minimumLegalCIEnabler;
    }

    public BigDecimal getDesirableLegalCIEnabler() {
        return desirableLegalCIEnabler;
    }

    public void setDesirableLegalCIEnabler(BigDecimal desirableLegalCIEnabler) {
        this.desirableLegalCIEnabler = desirableLegalCIEnabler;
    }

    public BigDecimal getMinimumLegalCIEmployer() {
        return minimumLegalCIEmployer;
    }

    public void setMinimumLegalCIEmployer(BigDecimal minimumLegalCIEmployer) {
        this.minimumLegalCIEmployer = minimumLegalCIEmployer;
    }

    public BigDecimal getDesirableLegalCIEmployer() {
        return desirableLegalCIEmployer;
    }

    public void setDesirableLegalCIEmployer(BigDecimal desirableLegalCIEmployer) {
        this.desirableLegalCIEmployer = desirableLegalCIEmployer;
    }

    public Integer getLimitContractingAgeYear() {
        return limitContractingAgeYear;
    }

    public void setLimitContractingAgeYear(Integer limitContractingAgeYear) {
        this.limitContractingAgeYear = limitContractingAgeYear;
    }

    public Integer getLimitContractingAgeMonth() {
        return limitContractingAgeMonth;
    }

    public void setLimitContractingAgeMonth(Integer limitContractingAgeMonth) {
        this.limitContractingAgeMonth = limitContractingAgeMonth;
    }

    public Integer getLimitContractingAgeDay() {
        return limitContractingAgeDay;
    }

    public void setLimitContractingAgeDay(Integer limitContractingAgeDay) {
        this.limitContractingAgeDay = limitContractingAgeDay;
    }

    public Short getMinimumDailyJourney() {
        return minimumDailyJourney;
    }

    public void setMinimumDailyJourney(Short minimumDailyJourney) {
        this.minimumDailyJourney = minimumDailyJourney;
    }

    public Short getMaximumDailyJourneyElementarySchool() {
        return maximumDailyJourneyElementarySchool;
    }

    public void setMaximumDailyJourneyElementarySchool(Short maximumDailyJourneyElementarySchool) {
        this.maximumDailyJourneyElementarySchool = maximumDailyJourneyElementarySchool;
    }

    public Short getMaximumDailyJourneySecondarySchool() {
        return maximumDailyJourneySecondarySchool;
    }

    public void setMaximumDailyJourneySecondarySchool(Short maximumDailyJourneySecondarySchool) {
        this.maximumDailyJourneySecondarySchool = maximumDailyJourneySecondarySchool;
    }

    public Integer getMinimumContractDays() {
        return minimumContractDays;
    }

    public void setMinimumContractDays(Integer minimumContractDays) {
        this.minimumContractDays = minimumContractDays;
    }

    public Integer getMinimumAmountActivities() {
        return minimumAmountActivities;
    }

    public void setMinimumAmountActivities(Integer minimumAmountActivities) {
        this.minimumAmountActivities = minimumAmountActivities;
    }

    public Integer getMaximumAmountActivities() {
        return maximumAmountActivities;
    }

    public void setMaximumAmountActivities(Integer maximumAmountActivities) {
        this.maximumAmountActivities = maximumAmountActivities;
    }

    public Integer getCancelationDeadline() {
        return cancelationDeadline;
    }

    public void setCancelationDeadline(Integer cancelationDeadline) {
        this.cancelationDeadline = cancelationDeadline;
    }

    public Integer getTerminationLetterDeadline() {
        return terminationLetterDeadline;
    }

    public void setTerminationLetterDeadline(Integer terminationLetterDeadline) {
        this.terminationLetterDeadline = terminationLetterDeadline;
    }

    public Integer getDaysAnticipationTermination() {
        return daysAnticipationTermination;
    }

    public void setDaysAnticipationTermination(Integer daysAnticipationTermination) {
        this.daysAnticipationTermination = daysAnticipationTermination;
    }

    public Boolean isElementarySchoolCourse() {
        return elementarySchoolCourse;
    }

    public void setElementarySchoolCourse(Boolean elementarySchoolCourse) {
        this.elementarySchoolCourse = elementarySchoolCourse;
    }

    public Boolean isSecondarySchoolCourse() {
        return secondarySchoolCourse;
    }

    public void setSecondarySchoolCourse(Boolean secondarySchoolCourse) {
        this.secondarySchoolCourse = secondarySchoolCourse;
    }

    public Boolean isHighEducationCourse() {
        return highEducationCourse;
    }

    public void setHighEducationCourse(Boolean highEducationCourse) {
        this.highEducationCourse = highEducationCourse;
    }

    public Boolean isTechnicalCourse() {
        return technicalCourse;
    }

    public void setTechnicalCourse(Boolean technicalCourse) {
        this.technicalCourse = technicalCourse;
    }

    public Integer getFundamentalApprenticeSchool() {
        return fundamentalApprenticeSchool;
    }

    public void setFundamentalApprenticeSchool(Integer fundamentalApprenticeSchool) {
        this.fundamentalApprenticeSchool = fundamentalApprenticeSchool;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.code == null) ? 0 : this.code.hashCode());
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final ApprenticeProgramParameter other = (ApprenticeProgramParameter) obj;
        if (this.code == null) {
            if (other.code != null) return false;
        } else if (!this.code.equals(other.code)) return false;
        if (this.id == null) {
            if (other.id != null) return false;
        } else if (!this.id.equals(other.id)) return false;
        return true;
    }

}


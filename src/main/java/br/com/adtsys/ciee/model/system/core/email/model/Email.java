package br.com.adtsys.ciee.model.system.core.email.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Entidade para persistência de Emails
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "EMAILS")
public class Email extends AbstractEntity {

    private static final long serialVersionUID = -5863663159031210867L;

    @Id
    @GeneratedValue(generator = "custom-sequence", strategy = GenerationType.SEQUENCE)
    private Long id;

    //@org.hibernate.validator.constraints.Email

    @Size(message = "'Endereço' deve ter tamanho máximo de 100 caracteres", max = 100)
    @Column(name = "ENDERECO", nullable = false, length = 100)
    private String address;

    @Size(message = "'Descrição' deve ter tamanho máximo de 100 caracteres", max = 100)
    @Column(name = "DESCRICAO", length = 100)
    private String description;
}
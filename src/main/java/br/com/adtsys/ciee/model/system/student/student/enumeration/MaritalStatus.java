package br.com.adtsys.ciee.model.system.student.student.enumeration;

public enum MaritalStatus {

    SINGLE("SOLTEIRO"),
    MARRIED("CASADO"),
    SEPARATED("SEPARADO"),
    DIVORCED("DIVORCIADO"),
    WIDOWER("VIUVO");

    private String maritalStatus;

    private MaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }
}

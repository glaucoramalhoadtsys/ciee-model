package br.com.adtsys.ciee.model.test.school.enums;

public enum InstitutionTypes {
    Particular,
    Municipal,
    Estadual,
    Federal
}
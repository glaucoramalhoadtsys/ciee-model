package br.com.adtsys.ciee.model.system.student.phone.enumeration;

public enum PhoneTypeEnum {
    LANDLINE("FIXO"),
    MOBILE("CELULAR");

    private String type;

    PhoneTypeEnum(String type) {
        this.type = type;
    }

    public String getTranslateType() {
        return this.type;
    }

}

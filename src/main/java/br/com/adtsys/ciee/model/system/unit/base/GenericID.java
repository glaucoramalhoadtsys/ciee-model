package br.com.adtsys.ciee.model.system.unit.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * Represents a gerenic key, value object to send data.
 * @author adtsys
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GenericID {

    private String id;
}

package br.com.adtsys.ciee.model.system.student.student.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Entitade que representa o progresso do cadastro do estudante
 */

public class StudentRegisterProgress {

    private BigDecimal registerCompletionPercentage;

    private List<StudentRegisterModule> pendingItems;

    public void addPendingItem(StudentRegisterModule registerItem) {
        if (Objects.isNull(pendingItems))
            pendingItems = new ArrayList<>();

        pendingItems.add(registerItem);
    }
}

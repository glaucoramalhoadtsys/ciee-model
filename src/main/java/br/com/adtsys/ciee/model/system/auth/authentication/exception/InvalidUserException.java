package br.com.adtsys.ciee.model.system.auth.authentication.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

/**
 * Created by adtsys on 6/8/17.
 */
@ResponseStatus(value = UNAUTHORIZED, reason = "Usuario inválido!")
public class InvalidUserException extends RuntimeException {

    private static final long serialVersionUID = -7219630541050283614L;

    public InvalidUserException() {
        super();
    }

    public InvalidUserException(final String message) {
        super(message);
    }
}

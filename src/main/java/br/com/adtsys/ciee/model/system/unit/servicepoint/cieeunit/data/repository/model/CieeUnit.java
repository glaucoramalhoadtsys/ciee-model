package br.com.adtsys.ciee.model.system.unit.servicepoint.cieeunit.data.repository.model;

import br.com.adtsys.ciee.model.system.unit.address.data.repository.model.Address;
import br.com.adtsys.ciee.model.system.unit.email.data.repository.model.Email;
import br.com.adtsys.ciee.model.system.unit.management.data.repository.model.Management;
import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import br.com.adtsys.ciee.model.system.unit.phone.data.repository.model.Phone;
import br.com.adtsys.ciee.model.system.unit.unit.data.repository.model.Unit;

import java.util.Set;

public class CieeUnit {
  private Long id;

  private Unit unit;

  private String description;

  private String shortDescription;

  private Address address;

  private Set<Phone> contactPhones;

  private Set<Phone> financeAreaPhones;

  private String cnpj;

  private Person responsible;

  private Set<Email> supportEmails;

  private Management management;

  private String crInternshipActivityCode;

  private String crPeActivityCode;

  private String crApprenticeActivityCode;

  private String crSecureCode;

  private String clientCode;

  private Boolean portalVisible;

  private Boolean active;


}

package br.com.adtsys.ciee.model.system.student.registerprogress.model;

import br.com.adtsys.ciee.model.system.student.student.model.Student;

import java.math.BigDecimal;

/**
 * Entidade que representa o percentual de completude do cadastro de um estudante
 */

public class RegisterProgress {


    private Long id;

    private Student student;

    private BigDecimal percentage;

}

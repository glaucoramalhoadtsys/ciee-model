package br.com.adtsys.ciee.model.system.unit.base.exception;

public class TableNotEmptyException extends RuntimeException {

    private static final long serialVersionUID = 1959893851526810748L;

    public TableNotEmptyException() {
        super("Impossível realizar importação; já existem dados na tabela!");
    }
}

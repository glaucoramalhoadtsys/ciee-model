package br.com.adtsys.ciee.model.system.auth.token.model;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DocumentSignatureTokenDTO {


    private String documentKey;


    private String name;


    private String email;


    private String documentationNumber;


    private Long idEducationInstitution;


    private String phoneNumber;

}

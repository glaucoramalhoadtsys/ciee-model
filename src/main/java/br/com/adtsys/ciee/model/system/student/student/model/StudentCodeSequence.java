package br.com.adtsys.ciee.model.system.student.student.model;


/**
 * Modelo que armazena a última sequência utilizada na geração de
 * códigos de estudante.
 */

public class StudentCodeSequence {

    private Long id;

    private String prefix;

    private Integer sequenceNumber;
}

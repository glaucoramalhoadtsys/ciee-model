package br.com.adtsys.ciee.model.system.auth.login.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class LoginTypeAlreadyExistsException extends RuntimeException {

    public LoginTypeAlreadyExistsException(String message) {
        super(message);
    }
}

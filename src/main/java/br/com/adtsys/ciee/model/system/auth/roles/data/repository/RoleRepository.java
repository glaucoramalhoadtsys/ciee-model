package br.com.adtsys.ciee.model.system.auth.roles.data.repository;

import br.com.adtsys.ciee.model.system.auth.roles.data.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByDescription(final String description);
}

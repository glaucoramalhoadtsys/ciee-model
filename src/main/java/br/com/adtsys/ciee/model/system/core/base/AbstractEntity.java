package br.com.adtsys.ciee.model.system.core.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.ZonedDateTime;
import java.util.Objects;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractEntity implements Persistable<Long> {

    private static final long serialVersionUID = 4139229304466323467L;

    @Column(name = "data_criacao", nullable = false, updatable = false)
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime creationDate;

    @Column(name = "data_alteracao", nullable = false)
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime modifiedDate;

    @Column(name = "modificado_por")
    @LastModifiedBy
    @JsonIgnore
    private String modifiedBy;

    @Column(name = "criado_por")
    @CreatedBy
    @JsonIgnore
    private String createdBy;

    @Column(name = "deletado", nullable = false)
    @JsonIgnore
    private Boolean deleted = false;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public abstract Long getId();

    public Boolean isDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    @JsonIgnore
    public boolean isNew() {
        return Objects.isNull(this.getId());
    }
}

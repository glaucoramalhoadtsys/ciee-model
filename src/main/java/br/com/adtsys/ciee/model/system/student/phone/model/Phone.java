package br.com.adtsys.ciee.model.system.student.phone.model;

import br.com.adtsys.ciee.model.system.student.phone.enumeration.PhoneTypeEnum;
import br.com.adtsys.ciee.model.system.student.student.model.StudentResource;
import org.apache.commons.lang3.BooleanUtils;

/**
 * Entidade que representa um Telefone.
 */

public class Phone extends StudentResource {

    private Long id;

    private String phone;

    private Boolean main;

    private Boolean verified;

    private PhoneTypeEnum phoneType;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isMain() {
        return BooleanUtils.isTrue(this.getMain());
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getMain() {
        return main;
    }

    public void setMain(Boolean main) {
        this.main = main;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public PhoneTypeEnum getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(PhoneTypeEnum phoneType) {
        this.phoneType = phoneType;
    }
}

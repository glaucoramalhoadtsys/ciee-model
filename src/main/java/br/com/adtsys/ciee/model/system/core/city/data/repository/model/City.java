package br.com.adtsys.ciee.model.system.core.city.data.repository.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import br.com.adtsys.ciee.model.system.core.state.repository.model.State;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Builder
@Entity(name = "municipios")
public class City extends AbstractEntity {

    private static final int CSV_STATE_INITIALS = 0;
    private static final int CSV_STATE_DESCRIPTION = 1;
    private static final int CSV_CITY_CODE = 2;
    private static final int CSV_CITY_NAME = 3;
    private static final int CSV_CITY_ACTIVE = 4;

    private static final long serialVersionUID = 3416406342666267700L;
    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "codigo_municipio", nullable = false)
    private Long cityCode;
    @Size(message = "Nome do município possui limite máximo de 150 caracteres", max = 150)
    @Column(name = "nome_municipio", nullable = false, length = 150)
    private String cityName;
    @Column(name = "ativo", nullable = false)
    private Boolean active;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "id_estado", nullable = false)
    private State state;

    @Tolerate
    public City() {
    }

    public Long getCityCode() {
        return this.cityCode;
    }

    public void setCityCode(final Long cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return this.cityName;
    }

    public void setCityName(final String cityName) {
        this.cityName = cityName;
    }

    public Boolean isActive() {
        return this.active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }

    public State getState() {
        return this.state;
    }

    public void setState(final State state) {
        this.state = state;
    }


    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.cityCode == null) ? 0 : this.cityCode.hashCode());
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final City other = (City) obj;
        if (this.cityCode == null) {
            if (other.cityCode != null) return false;
        } else if (!this.cityCode.equals(other.cityCode)) return false;
        if (this.id == null) {
            if (other.id != null) return false;
        } else if (!this.id.equals(other.id)) return false;
        return true;
    }
}

package br.com.adtsys.ciee.model.system.core.educationdegree.data.repository;

import br.com.adtsys.ciee.model.system.core.educationdegree.data.repository.model.EducationDegree;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class EducationDegreeSpecification implements Specification<EducationDegree> {
    private final String description;

    public EducationDegreeSpecification(String description) {
        this.description = description;
    }

    @Override
    public Predicate toPredicate(Root<EducationDegree> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        final Predicate predicate = cb.conjunction();

        if (Strings.isNotBlank(this.description)) {
            predicate.getExpressions().add(cb.like(cb.lower(root.get("description")), "%" + this.description.toLowerCase() + "%"));
        }

        return predicate;
    }
}

package br.com.adtsys.ciee.model.system.student.enrollmentpermissions.model;

public class EnrollmentPermission {


    private Long id;

    private String state;

    private String educationDegree;

    private Boolean allowed;

    private String link;

    private Boolean active = true;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

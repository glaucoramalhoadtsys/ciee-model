package br.com.adtsys.ciee.model.system.student.studentadditionalinformation.model;

public class StudentAdditionalInformation {


    private Long id;

    private Long genderId;

    private Long ethnicityId;

    private Boolean cnh;

    private Boolean children;

    private Boolean smoker;
}

package br.com.adtsys.ciee.model.system.unit.security.exceptions;


public class JwtExpirationException extends RuntimeException {
    public JwtExpirationException(final String message) {
        super(message);
    }
}

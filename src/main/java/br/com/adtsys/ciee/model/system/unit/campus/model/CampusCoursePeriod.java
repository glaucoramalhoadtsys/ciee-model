package br.com.adtsys.ciee.model.system.unit.campus.model;


import br.com.adtsys.ciee.model.system.unit.base.enumeration.ClassScheduleTypeEnum;
import br.com.adtsys.ciee.model.system.unit.base.enumeration.CourseDurationTypeEnum;
import br.com.adtsys.ciee.model.system.unit.base.enumeration.CoursePeriodTypeEnum;
import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Entity
@Table(name = "campus_cursos_periodos")
@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CampusCoursePeriod {
    private static final long serialVersionUID = 7690842863167860374L;

    @Id
    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;

    @JsonBackReference
    @ManyToOne(optional = false, cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "id_campus_curso")
    private CampusCourse campusCourse;

    @NotNull(message = "'Tipo de duração' deve ser preenchido")
    @Column(name = "tipo_duracao_curso", nullable = false)
    private CourseDurationTypeEnum durationType;

    @NotNull(message = "'Tipo de período' deve ser preenchido")
    @Column(name = "tipo_periodo_curso", nullable = false)
    private CoursePeriodTypeEnum periodType;

    @NotNull(message = "'Duração do curso' deve ser preenchida")
    @Column(name = "duracao_curso", nullable = false)
    @Min(value = 1, message = "O valor da duração do curso deve ser positivo.")
    private Integer duration;

    @NotNull(message = "'Tipo de Horário de Aula' deve ser preenchida")
    @Column(name = "tipo_horario_aula", nullable = false)
    private ClassScheduleTypeEnum classScheduleType;

    @Column(name = "horario_inicio_aula")
    @JsonFormat(pattern = "HH:mm")
    private LocalTime classStartAt;

    @Column(name = "horario_fim_aula")
    @JsonFormat(pattern = "HH:mm")
    private LocalTime classFinishAt;

    @NotNull(message = "'Estagio autorizado a partir de' deve ser preenchido")
    @Column(name = "estagio_apos_tempo", nullable = false)
    @Min(value = 1, message = "O valor de 'estágio autorizado a partir de' deve ser positivo.")
    private Integer internshipAfterTime;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "id_responsavel")
    private Person responsible;

    @Transient
    private Long responsibleID;

    @JsonIgnore
    @Builder.Default
    @Column(name = "bloqueado")
    private Boolean blocked = false;

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public Long getResponsibleID() {
        return responsibleID;
    }

    @JsonProperty
    public void setResponsibleID(Long responsibleId) {
        this.responsibleID = responsibleId;
    }
}

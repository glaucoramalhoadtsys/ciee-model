package br.com.adtsys.ciee.model.system.student.skill.computing.enumeration;

/**
 * Níveis de conhecimento em informática
 */
public enum ComputingSkillLevelEnum {

    BASIC("BASICO"),
    INTERMEDIATE("INTERMEDIARIO"),
    ADVANCED("AVANCADO");

    private String level;

    private ComputingSkillLevelEnum(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }

}

package br.com.adtsys.ciee.model.system.student.curriculumtemplate.model;

public class CurriculumTemplate {


    private Long id;

    private String name;

    private String description;

    private Long curriculumTemplateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

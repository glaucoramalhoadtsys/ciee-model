package br.com.adtsys.ciee.model.system.student.registerprogress.enumeration;

import java.math.BigDecimal;

/**
 * Classificação do cadastro de estudante de acordo com percentual de completude
 */
public enum RegisterProgressEnum {

    WEAK(new BigDecimal("0"), new BigDecimal("54.99")),
    REGULAR(new BigDecimal("55"), new BigDecimal("74.99")),
    GOOD(new BigDecimal("75"), new BigDecimal("84.99")),
    GREAT(new BigDecimal("85"), new BigDecimal("99.99")),
    EXCELLENT(new BigDecimal("100"), new BigDecimal("100"));

    private BigDecimal min;
    private BigDecimal max;

    private RegisterProgressEnum(BigDecimal min, BigDecimal max) {
        this.min = min;
        this.max = max;
    }

    public BigDecimal getMin() {
        return min;
    }

    public BigDecimal getMax() {
        return max;
    }
}
 
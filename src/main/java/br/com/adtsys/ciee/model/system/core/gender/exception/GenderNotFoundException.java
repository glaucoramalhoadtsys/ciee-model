package br.com.adtsys.ciee.model.system.core.gender.exception;

import br.com.adtsys.ciee.model.system.core.base.exception.NotFoundException;

public class GenderNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 1L;

    private static final String NOT_FOUND_MESSAGE = "Gênero %s não encontrado";

    public GenderNotFoundException(Long id) {
        super(String.format(NOT_FOUND_MESSAGE, id));
    }

}

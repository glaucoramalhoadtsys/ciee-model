package br.com.adtsys.ciee.model.system.student.student.model;

import br.com.adtsys.ciee.model.system.student.studentschooling.model.StudentSchoolingDTO;

/**
 * DTO que contém dados detalhados de um estudante, usado no envio de dados para o legado.
 */

public class StudentLegacyDTO {

    private Student student;
    private StudentSchoolingDTO schooling;

}

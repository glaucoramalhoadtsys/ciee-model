package br.com.adtsys.ciee.model.system.auth.user.data.model;

public enum UserType {

    BACKOFFICE("BACKOFFICE"),
    SCHOOL("ESCOLA");

    private String key;

    private UserType(String key) {
        this.key = key;
    }

    private String getKey() {
        return key;
    }
}

package br.com.adtsys.ciee.model.system.auth.token.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TokenDataDTO {

    private String claimName;

    private String claimValue;

}

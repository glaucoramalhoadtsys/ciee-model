package br.com.adtsys.ciee.model.system.core.state.repository.model;

import br.com.adtsys.ciee.model.system.core.address.model.Address;
import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Builder
@Entity(name = "estados")
//@AttributeOverride(name = "id", column = @Column(name = "id"))
public class State extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 6530999855548133434L;
    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Size(message = "Sigla do estado possui limite máximo de 2 caracteres", max = 2, min = 2)
    @Column(name = "sigla_estado", nullable = false, length = 2)
    private String initials;
    @Size(message = "Descrição do estado possui limite máximo de 50 caracteres", max = 50)
    @Column(name = "descricao_estado", nullable = false, length = 50)
    private String description;
    @Column(name = "ativo", nullable = false)
    private Boolean active;
    @OneToMany
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
    private List<Address> addresses;

    @Tolerate
    public State() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInitials() {
        return this.initials;
    }

    public void setInitials(final String initials) {
        this.initials = initials;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Boolean isActive() {
        return this.active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = prime * result + ((this.initials == null) ? 0 : this.initials.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final State other = (State) obj;
        if (this.id == null) {
            if (other.id != null) return false;
        } else if (!this.id.equals(other.id)) return false;
        if (this.initials == null) {
            if (other.initials != null) return false;
        } else if (!this.initials.equals(other.initials)) return false;
        return true;
    }
}

package br.com.adtsys.ciee.model.system.core.phone.converter;

import br.com.adtsys.ciee.model.system.core.phone.enums.PhoneTypeEnum;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import java.util.Objects;

public class PhoneTypeConverter implements AttributeConverter<PhoneTypeEnum, String> {

    @Override
    public String convertToDatabaseColumn(PhoneTypeEnum phoneType) {
        return Objects.nonNull(phoneType) ? phoneType.getPhoneType() : null;
    }

    @Override
    public PhoneTypeEnum convertToEntityAttribute(String phoneType) {
        if (StringUtils.isNotBlank(phoneType)) {
            return Arrays.stream(PhoneTypeEnum.values()).filter(e -> e.getPhoneType().equalsIgnoreCase(phoneType)).findFirst().orElse(null);
        }
        return null;
    }
}

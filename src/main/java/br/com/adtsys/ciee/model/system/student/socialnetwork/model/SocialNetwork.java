package br.com.adtsys.ciee.model.system.student.socialnetwork.model;

/**
 * Entidade que armazena informações referentes
 * a rede social.
 */

public class SocialNetwork {


    private Long id;

    private String facebookLink;

    private String googlePlusLink;

    private String skypeLink;

    private String linkedinLink;

    public Long getId() {
        return this.id;
    }

    public void setId(Long idValue) {
        this.id = idValue;
    }

}

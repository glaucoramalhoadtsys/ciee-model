package br.com.adtsys.ciee.model.system.core.parametertype.data.repository.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Builder
@Entity(name = "tipos_parametros")
public class ParameterType extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 3537174668107217496L;

    @Id
    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;


    @Size(message = "'Sigla do Tipo de Parametro' possui tamanho máximo de 1 caractere.", max = 1, min = 1)
    @Column(name = "sigla", length = 1, nullable = false)
    private Character initials;


    @Size(message = "'Descrição do Tipo de Parametro' possui tamanho máximo de 20 caracteres.", max = 20)
    @Column(name = "descricao", length = 20, nullable = false)
    private String description;

    @Tolerate
    public ParameterType() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Character getInitials() {
        return this.initials;
    }

    public void setInitials(final Character initials) {
        this.initials = initials;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = prime * result + ((this.initials == null) ? 0 : this.initials.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final ParameterType other = (ParameterType) obj;
        if (this.id == null) {
            if (other.id != null) return false;
        } else if (!this.id.equals(other.id)) return false;
        if (this.initials == null) {
            if (other.initials != null) return false;
        } else if (!this.initials.equals(other.initials)) return false;
        return true;
    }

}

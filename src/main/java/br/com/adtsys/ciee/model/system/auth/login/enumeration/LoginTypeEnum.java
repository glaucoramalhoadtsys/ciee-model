package br.com.adtsys.ciee.model.system.auth.login.enumeration;

public enum LoginTypeEnum {

    FACEBOOK("FACEBOOK"),
    KAIROS("KAIROS"),
    LDAP("LDAP"),
    GOOGLE("GOOGLE");

    private String type;

    private LoginTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}

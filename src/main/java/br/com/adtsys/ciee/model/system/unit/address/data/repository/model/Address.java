package br.com.adtsys.ciee.model.system.unit.address.data.repository.model;

public class Address {

    // private Long id;

    // private String address;

    // private String type;

    private String number;

    // private String complement;

    // private String neighborhood;

    private String zipCode;


    // private String city;


    private String state;

    public Address() {
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

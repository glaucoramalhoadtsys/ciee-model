package br.com.adtsys.ciee.model.system.student.essaytheme.model;

public class EssayTheme {

    private Long id;

    private String description;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

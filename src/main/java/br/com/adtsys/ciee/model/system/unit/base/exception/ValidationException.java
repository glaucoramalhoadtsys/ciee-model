package br.com.adtsys.ciee.model.system.unit.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class ValidationException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = -7519286911136519829L;

    public ValidationException() {
        this("Erros de validação impedem de processar a requisição!");
    }

    public ValidationException(String msg) {
        super(msg);
    }
}

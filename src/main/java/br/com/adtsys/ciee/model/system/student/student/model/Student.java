package br.com.adtsys.ciee.model.system.student.student.model;

import br.com.adtsys.ciee.model.system.student.address.model.Address;
import br.com.adtsys.ciee.model.system.student.converter.LocalDateAttributeConverter;
import br.com.adtsys.ciee.model.system.student.email.model.Email;
import br.com.adtsys.ciee.model.system.student.nationality.model.NationalityInformation;
import br.com.adtsys.ciee.model.system.student.phone.model.Phone;
import br.com.adtsys.ciee.model.system.student.student.enumeration.DataValidationStatus;
import br.com.adtsys.ciee.model.system.student.student.enumeration.MaritalStatus;
import br.com.adtsys.ciee.model.system.student.student.enumeration.Sex;
import br.com.adtsys.ciee.model.system.student.student.enumeration.StudentProgramTypeEnum;
import br.com.adtsys.ciee.model.system.student.studentblockingreason.model.StudentBlockingReason;
import br.com.adtsys.ciee.model.system.student.studentschooling.model.StudentSchooling;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import javax.persistence.Convert;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * Entidade que armazena informações de um estudante.
 */

public class Student implements Serializable {


    private Long id;

    private String code;

    /*private List<StudentLanguage> languages;*/

    private String name;

    /*private String revenueName;*/

    private String cpf;

    private DataValidationStatus dataValidationStatus = DataValidationStatus.PENDENTE;

    private String dataValidationReason;

    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate birthDate;

    /*private LocalDate revenueBirthDate;*/

    private String socialName;

    private Boolean useSocialName;

    /*private Boolean hasLearningProgram;*/

    /**
     * O Atributo Sexo foi traduzido em seu sentido biológico para o inglês (sex) de acordo com o dicionário oxford,
     *
     * @see <a href="https://en.oxforddictionaries.com/definition/us/gender">Definição de Gênero</a>
     * pelo fato do sistema contemplar outras identidades de gênero.
     */

    private Sex sex;

    private NationalityInformation nationalityInformation;

    /*private WorkRegistration workRegistration;

    private SocialNetwork socialNetwork;*/

    private List<Address> addresses;

    private List<Phone> phones;

    private List<Email> emails;

    /*private List<VoluntaryActivity> voluntaryActivities;

    private SocialInformation socialInformation;

    private List<ProfessionalExperience> professionalExperiences;

    private List<Dependent> dependents;*/

    private Boolean acceptsSms;

    private MaritalStatus maritalStatus;

    /*private Set<ComputingSkill> computingSkills;

    private Set<OtherSkill> otherSkills;*/

    private List<StudentSchooling> schoolings;

    // private StudentEnem enem;

    // private StudentSituationEnum situation;

    /*private Boolean pcd;

    private Set<MedicalReport> pcdMedicalReports;

    private Set<Appliance> pcdAppliances;

    private String pcdAdditionalDetails;

    private Boolean pcdEligibility = false;*/

    private Long authUserId;

    private String username;

    private String password;

    private List<StudentBlockingReason> studentsBlockingReasons;

    private Long profilePhotoId;

    private Long curriculumId;

    private String videoUrl;

    /*private PCDAnalysisStatusEnum pcdAnalysisStatus;

    private Set<Cid> pcdCids;

    private String pcdBoAdditionalDetails;

    private PCDDisabilityTypeEnum pcdDisabilityType;

    private Boolean pcdUseAppliances;*/

    /*private Responsible responsible;

    private StudentAdditionalInformation studentAdditionalInformation;

    private List<Essay> essays;*/

    private StudentProgramTypeEnum programType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public DataValidationStatus getDataValidationStatus() {
        return dataValidationStatus;
    }

    public void setDataValidationStatus(DataValidationStatus dataValidationStatus) {
        this.dataValidationStatus = dataValidationStatus;
    }

    public String getDataValidationReason() {
        return dataValidationReason;
    }

    public void setDataValidationReason(String dataValidationReason) {
        this.dataValidationReason = dataValidationReason;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getSocialName() {
        return socialName;
    }

    public void setSocialName(String socialName) {
        this.socialName = socialName;
    }

    public Boolean getUseSocialName() {
        return useSocialName;
    }

    public void setUseSocialName(Boolean useSocialName) {
        this.useSocialName = useSocialName;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public NationalityInformation getNationalityInformation() {
        return nationalityInformation;
    }

    public void setNationalityInformation(NationalityInformation nationalityInformation) {
        this.nationalityInformation = nationalityInformation;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public List<Email> getEmails() {
        return emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }

    public Boolean getAcceptsSms() {
        return acceptsSms;
    }

    public void setAcceptsSms(Boolean acceptsSms) {
        this.acceptsSms = acceptsSms;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public List<StudentSchooling> getSchoolings() {
        return schoolings;
    }

    public void setSchoolings(List<StudentSchooling> schoolings) {
        this.schoolings = schoolings;
    }

    public Long getAuthUserId() {
        return authUserId;
    }

    public void setAuthUserId(Long authUserId) {
        this.authUserId = authUserId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<StudentBlockingReason> getStudentsBlockingReasons() {
        return studentsBlockingReasons;
    }

    public void setStudentsBlockingReasons(List<StudentBlockingReason> studentsBlockingReasons) {
        this.studentsBlockingReasons = studentsBlockingReasons;
    }

    public Long getProfilePhotoId() {
        return profilePhotoId;
    }

    public void setProfilePhotoId(Long profilePhotoId) {
        this.profilePhotoId = profilePhotoId;
    }

    public Long getCurriculumId() {
        return curriculumId;
    }

    public void setCurriculumId(Long curriculumId) {
        this.curriculumId = curriculumId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public StudentProgramTypeEnum getProgramType() {
        return programType;
    }

    public void setProgramType(StudentProgramTypeEnum programType) {
        this.programType = programType;
    }
}

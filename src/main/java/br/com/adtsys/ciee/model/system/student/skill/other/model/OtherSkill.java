package br.com.adtsys.ciee.model.system.student.skill.other.model;


import br.com.adtsys.ciee.model.system.student.student.model.Student;

import java.time.LocalDate;

/**
 * Entidade de conhecimentos diversos
 */

public class OtherSkill {


    private Long id;

    private Student student;

    private String courseName;

    private String institutionName;

    private Integer workload;

    private LocalDate startDate;

    private LocalDate endDate;

    private Long documentId;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

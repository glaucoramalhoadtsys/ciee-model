package br.com.adtsys.ciee.model.system.unit.campus.model;

import lombok.Data;

import java.util.Map;

@Data
public class CampusCoursePeriodDTO {

    private Campus campus;
    private CampusCourse campusCourse;
    private CampusCoursePeriod campusCoursePeriod;

    public CampusCoursePeriodDTO(CampusCoursePeriod campusCoursePeriod, Map<Long, String> coursesMap) {
        this.setCampus(campusCoursePeriod.getCampusCourse().getCampus());
        this.setCampusCourse(campusCoursePeriod.getCampusCourse());
        this.campusCourse.setCourseName(coursesMap.get(campusCoursePeriod.getCampusCourse().getCourseId()));
        this.setCampusCoursePeriod(campusCoursePeriod);
    }

    public CampusCoursePeriodDTO(CampusCoursePeriod campusCoursePeriod) {
        this.setCampusCoursePeriod(campusCoursePeriod);
        this.setCampusCourse(campusCoursePeriod.getCampusCourse());
        this.setCampus(campusCoursePeriod.getCampusCourse().getCampus());
    }

}

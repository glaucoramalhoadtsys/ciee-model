package br.com.adtsys.ciee.model.system.unit.base.exception;

public class ClientException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private static final String MESSAGE = "Erro de comunicação com o microserviço: %s ";

    public ClientException(String message) {
        super(String.format(MESSAGE, message));
    }

}

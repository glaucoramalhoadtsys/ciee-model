package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ie_pre_cad_checklist")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PreRegistrationCheckListItem extends RegistrationCheckListItem {

    private static final long serialVersionUID = 7273574356121959738L;

    @ManyToOne(optional = false)
    @JoinColumn(name = "id_pre_cadastro")
    private EIPreRegistration preRegistration;
}

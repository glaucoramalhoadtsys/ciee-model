package br.com.adtsys.ciee.model.system.core.security.exceptions;

import org.springframework.security.core.AuthenticationException;

public class JwtAuthenticationException extends AuthenticationException {

    private static final long serialVersionUID = -16267265030416576L;

    public JwtAuthenticationException(final String msg) {
        super(msg);
    }

}

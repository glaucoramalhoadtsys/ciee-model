package br.com.adtsys.ciee.model.system.student.skill.computing.model;


import br.com.adtsys.ciee.model.system.student.skill.computing.enumeration.ComputingSkillLevelEnum;
import br.com.adtsys.ciee.model.system.student.skill.computing.enumeration.ComputingSkillTypeEnum;
import br.com.adtsys.ciee.model.system.student.student.model.Student;

/**
 * Entidade de conhecimentos em informática
 */

public class ComputingSkill {


    private Long id;

    private Student student;

    private String tool;

    private ComputingSkillTypeEnum type;

    private ComputingSkillLevelEnum level;

    private Long documentId;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

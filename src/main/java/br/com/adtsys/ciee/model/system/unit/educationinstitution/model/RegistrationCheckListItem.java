package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import br.com.adtsys.ciee.model.system.unit.base.AbstractEntity;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.converter.RegistrationCheckListItemStatusConverter;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.converter.RegistrationCheckListItemTypeConverter;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration.RegistrationCheckListItemStatus;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration.RegistrationCheckListItemType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "ie_cadastro_checklist_item")
@Inheritance(strategy = InheritanceType.JOINED)
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = false)
public class RegistrationCheckListItem extends AbstractEntity {
    private static final long serialVersionUID = 1209663893192300220L;

    @Id
    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "item_tipo", nullable = false)
    @Convert(converter = RegistrationCheckListItemTypeConverter.class)
    private RegistrationCheckListItemType itemType;

    @Column(name = "item_valor", nullable = true)
    private String itemValue;

    @Column(name = "status", nullable = false)
    @Convert(converter = RegistrationCheckListItemStatusConverter.class)
    private RegistrationCheckListItemStatus status = RegistrationCheckListItemStatus.PENDING;

    @Column(name = "observacao", nullable = true)
    private String comments;
}

package br.com.adtsys.ciee.model.test.school.prerequest;


import br.com.adtsys.ciee.model.system.unit.address.data.repository.model.Address;
import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;

import java.util.List;

public class SchoolPreAccessRequest {

    private Boolean shouldCreateCooperationAgreement;
    private Boolean active;
    private Address address;
    private String authorizationCode;
    private List<Person> contacts;
    private String course;
    private String institutionName;
    private String password;
    private Long institutionType;
    private String educationDegree;

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Boolean getShouldCreateCooperationAgreement() {
        return shouldCreateCooperationAgreement;
    }

    public void setShouldCreateCooperationAgreement(Boolean shouldCreateCooperationAgreement) {
        this.shouldCreateCooperationAgreement = shouldCreateCooperationAgreement;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public List<Person> getContacts() {
        return contacts;
    }

    public void setContacts(List<Person> contacts) {
        this.contacts = contacts;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(Long institutionType) {
        this.institutionType = institutionType;
    }

    public String getEducationDegree() {
        return educationDegree;
    }

    public void setEducationDegree(String educationDegree) {
        this.educationDegree = educationDegree;
    }
}
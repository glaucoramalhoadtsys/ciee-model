package br.com.adtsys.ciee.model.system.unit.base.converter;

import br.com.adtsys.ciee.model.system.unit.base.enumeration.CourseModalityEnum;

import javax.persistence.AttributeConverter;
import java.util.Objects;

public class CourseModalityConverter implements AttributeConverter<CourseModalityEnum, String> {

    @Override
    public String convertToDatabaseColumn(final CourseModalityEnum courseModality) {
        if (Objects.nonNull(courseModality)) {
            return courseModality.toString();
        }
        return null;
    }

    @Override
    public CourseModalityEnum convertToEntityAttribute(final String column) {
        for (final CourseModalityEnum courseModality : CourseModalityEnum.values()) {
            if (column.equals(courseModality.toString())) {
                return courseModality;
            }
        }
        return null;
    }
}

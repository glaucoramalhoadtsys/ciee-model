package br.com.adtsys.ciee.model.system.student.blockingreason.model;

import br.com.adtsys.ciee.model.system.student.student.enumeration.StudentSituationEnum;

public class BlockingReason {


    private Long id;

    private String initial;

    private String internalDescription;

    private String externalDescription;

    private Boolean active;

    private StudentSituationEnum situation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

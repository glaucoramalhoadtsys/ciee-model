package br.com.adtsys.ciee.model.system.student.language.model;

import br.com.adtsys.ciee.model.system.student.student.model.Student;

/**
 * Entidade que representa níveis de idiomas dos estudantes
 */

public class StudentLanguage {


    private Long id;

    private Student student;

    private Language language;

    private Level level;

    private Long documentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

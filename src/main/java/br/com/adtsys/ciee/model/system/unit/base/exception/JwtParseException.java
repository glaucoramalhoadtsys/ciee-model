package br.com.adtsys.ciee.model.system.unit.base.exception;

public class JwtParseException extends RuntimeException {
    private static final long serialVersionUID = -3344349164568731759L;

    public JwtParseException(String msg) {
        super(msg);
    }
}

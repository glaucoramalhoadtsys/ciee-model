package br.com.adtsys.ciee.model.system.auth.user.data.model;

import br.com.adtsys.ciee.model.system.auth.domain.data.repository.model.Domain;
import br.com.adtsys.ciee.model.system.auth.login.model.Login;
import br.com.adtsys.ciee.model.system.auth.roles.data.model.Role;

import java.util.Set;

/**
 * Entidade que representa um usuário no sistema de autenticação.
 */
public class User {

    private Long id;
    private String name;
    private String email;
    private String code;
    private String password;
    private Set<Domain> domainsUser;
    private Role role;
    private String userType;
    private String cpf;
    private Set<Login> logins;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Domain> getDomainsUser() {
        return domainsUser;
    }

    public void setDomainsUser(Set<Domain> domainsUser) {
        this.domainsUser = domainsUser;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Set<Login> getLogins() {
        return logins;
    }

    public void setLogins(Set<Login> logins) {
        this.logins = logins;
    }
}

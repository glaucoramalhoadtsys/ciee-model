package br.com.adtsys.ciee.model.system.core.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class JwtParseException extends RuntimeException {
    private static final long serialVersionUID = -3344349164568731759L;

    public JwtParseException(String msg) {
        super(msg);
    }
}

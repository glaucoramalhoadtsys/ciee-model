package br.com.adtsys.ciee.model.system.core.security.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type Token creation exception.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Problema na criação do Token")
public class TokenCreationException extends RuntimeException {

    private static final long serialVersionUID = -2520350332504530799L;
}

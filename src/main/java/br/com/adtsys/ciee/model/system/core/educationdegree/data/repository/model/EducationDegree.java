package br.com.adtsys.ciee.model.system.core.educationdegree.data.repository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Entidade que representa níveis de ensino.
 */
@Builder
@Entity(name = "NIVEIS_ENSINO")
@AllArgsConstructor
public class EducationDegree implements Serializable {

    private static final long serialVersionUID = -5073128862463486258L;

    @Id
    @Column(name = "ABREVIATURA", nullable = false, length = 5)
    private String initials;


    @Size(message = "'Descrição' possui limite máximo de 35 caracteres", max = 35)
    @Column(name = "DESCRICAO", nullable = false, length = 35)
    private String description;

    @JsonIgnore
    @Column(name = "ORDEM", unique = true)
    private Short order;

    @Tolerate
    public EducationDegree() {
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getInitials() {
        return this.initials;
    }

    public void setInitials(final String initials) {
        this.initials = initials;
    }

    public Short getOrder() {
        return this.order;
    }

    public void setOrder(Short order) {
        this.order = order;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.initials == null) ? 0 : this.initials.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final EducationDegree other = (EducationDegree) obj;
        if (this.initials == null) {
            if (other.initials != null) return false;
        } else if (!this.initials.equals(other.initials)) return false;
        return true;
    }

}

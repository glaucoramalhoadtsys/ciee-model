package br.com.adtsys.ciee.model.system.core.educationdegree.data.repository;

import java.util.*;
import java.util.stream.Collectors;

public enum EducationDegrees {

    SU("Superior"),
    TE("Técnico"),
    EE("Educação Especial"),
    HB("Habilitação Básica"),
    EM("Ensino Médio"),
    EF("Ensino Fundamental");

    private String description;

    EducationDegrees(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static String getRandomValue() {
        return values()[new Random().nextInt(values().length)].getDescription();
    }

    public static EducationDegrees getEnumFromValue(String value){
        return EnumSet.allOf(EducationDegrees.class).stream().filter(degree ->
            degree.getDescription().equals(value)
        ).collect(Collectors.toList()).get(0);

    }
}

package br.com.adtsys.ciee.model.system.student.studentschooling.model;

import br.com.adtsys.ciee.model.system.student.converter.LocalDateAttributeConverter;
import br.com.adtsys.ciee.model.system.student.student.model.Student;
import br.com.adtsys.ciee.model.system.student.studentschooling.enumeration.CourseDurationTypeEnum;
import br.com.adtsys.ciee.model.system.student.studentschooling.enumeration.CoursePeriodTypeEnum;
import br.com.adtsys.ciee.model.system.student.studentschooling.enumeration.SchoolingStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import javax.persistence.Convert;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Entidade que armazena informações de escolaridade de um estudante.
 */

public class StudentSchooling implements Serializable {

    private Long id;

    private Student student;

    private Long coursePeriodId;

    private Boolean oab;

    private String enrollment;

    private Short currentPeriod;

    private Short latestPeriod;

    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate endDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate expectedEndDate;

    private Boolean main;

    private String schoolName;

    private Long courseId;

    private String educationDegreeInitials;

    private CoursePeriodTypeEnum coursePeriodType;

    private Short courseDuration;

    private CourseDurationTypeEnum courseDurationType;

    private SchoolingStatusEnum schoolingStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Long getCoursePeriodId() {
        return coursePeriodId;
    }

    public void setCoursePeriodId(Long coursePeriodId) {
        this.coursePeriodId = coursePeriodId;
    }

    public Boolean getOab() {
        return oab;
    }

    public void setOab(Boolean oab) {
        this.oab = oab;
    }

    public String getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(String enrollment) {
        this.enrollment = enrollment;
    }

    public Short getCurrentPeriod() {
        return currentPeriod;
    }

    public void setCurrentPeriod(Short currentPeriod) {
        this.currentPeriod = currentPeriod;
    }

    public Short getLatestPeriod() {
        return latestPeriod;
    }

    public void setLatestPeriod(Short latestPeriod) {
        this.latestPeriod = latestPeriod;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(LocalDate expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public Boolean getMain() {
        return main;
    }

    public void setMain(Boolean main) {
        this.main = main;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getEducationDegreeInitials() {
        return educationDegreeInitials;
    }

    public void setEducationDegreeInitials(String educationDegreeInitials) {
        this.educationDegreeInitials = educationDegreeInitials;
    }

    public CoursePeriodTypeEnum getCoursePeriodType() {
        return coursePeriodType;
    }

    public void setCoursePeriodType(CoursePeriodTypeEnum coursePeriodType) {
        this.coursePeriodType = coursePeriodType;
    }

    public Short getCourseDuration() {
        return courseDuration;
    }

    public void setCourseDuration(Short courseDuration) {
        this.courseDuration = courseDuration;
    }

    public CourseDurationTypeEnum getCourseDurationType() {
        return courseDurationType;
    }

    public void setCourseDurationType(CourseDurationTypeEnum courseDurationType) {
        this.courseDurationType = courseDurationType;
    }

    public SchoolingStatusEnum getSchoolingStatus() {
        return schoolingStatus;
    }

    public void setSchoolingStatus(SchoolingStatusEnum schoolingStatus) {
        this.schoolingStatus = schoolingStatus;
    }
}

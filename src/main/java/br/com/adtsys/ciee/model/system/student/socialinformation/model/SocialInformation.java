package br.com.adtsys.ciee.model.system.student.socialinformation.model;

import br.com.adtsys.ciee.model.system.student.student.model.Student;
import org.apache.commons.lang3.BooleanUtils;

import java.math.BigDecimal;

/**
 * Entidade que representa as informações Sociais do estudante.
 */

public class SocialInformation {


    private Long id;

    private Student student;

    private Long metCieeProgramId;

    private BigDecimal monthlyIncome;

    private Integer peopleLivingTogether;

    private Boolean useGovernmentIncomeProgram;

    private Long governmentIncomeProgramId;

    private Boolean hasEducationalIncentive;

    private Long educationIncentiveId;

    private Boolean onSingleRegistration;

    private String singleRegistrationIdentifier;

    private Long singleRegistrationDocumentId;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isOnSingleRegistration() {
        return BooleanUtils.isTrue(this.onSingleRegistration);
    }
}

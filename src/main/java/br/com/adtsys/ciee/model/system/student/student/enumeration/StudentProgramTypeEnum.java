package br.com.adtsys.ciee.model.system.student.student.enumeration;

public enum StudentProgramTypeEnum {

    INTERNSHIP("ESTAGIO"),
    APPRENTICE("APRENDIZ"),
    BOTH("AMBOS");

    private String type;

    private StudentProgramTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}

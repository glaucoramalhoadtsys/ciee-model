package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration.PreRegistrationSituation;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ie_pre_cadastros")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class EIPreRegistration {

    private static final long serialVersionUID = -4953271668209240110L;

    @Id
    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne(optional = false)
    @JoinColumn(name = "id_instituicao_ensino")
    private EducationInstitution educationInstitution;

    @Column(name = "numero_protocolo")
    private String protocolNumber;

    private PreRegistrationSituation situation;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pre_cadastro", foreignKey = @ForeignKey(name = "FK_PRE_REG_CHKL_ITEM"))
    private List<PreRegistrationCheckListItem> preRegistrationChecklistItem;
}

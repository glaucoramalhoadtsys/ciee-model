package br.com.adtsys.ciee.model.system.auth.authentication.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@ResponseStatus(value = UNAUTHORIZED, reason = "Não foi possível validar seu acesso")
public class InvalidLdapUserException extends RuntimeException {
    public InvalidLdapUserException() {
        super();
    }

    public InvalidLdapUserException(final String message) {
        super(message);
    }
}

package br.com.adtsys.ciee.model.system.core.gender.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entidade representando a tabela de gêneros
 */
@Entity
@Table(name = "GENEROS")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Gender extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CODIGO_GENERO", nullable = false)

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Size(message = "'Descrição' possui limite máximo de 150 caracteres", max = 150)

    @Column(name = "DESCRICAO", nullable = false, length = 150)
    private String description;

    @NotNull(message = "'Ativo' é uma informação obrigatória")
    @Column(name = "ATIVO", nullable = false)
    private Boolean active;

    @Override
    public Long getId() {
        return this.id;
    }

}

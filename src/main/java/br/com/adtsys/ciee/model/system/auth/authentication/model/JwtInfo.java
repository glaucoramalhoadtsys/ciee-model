package br.com.adtsys.ciee.model.system.auth.authentication.model;

import lombok.Builder;
import lombok.Data;

/**
 * Classe que representa as informações contidas no JWT de autenticação do CIEE.
 */
@Data
@Builder
public class JwtInfo {
    private Long userId;
    private UserTypeEnum userType;

    /**
     * Cria builder com setter de userType recebendo string ao invés de Enum
     */
    public static class JwtInfoBuilder {
        private UserTypeEnum userType;

        public JwtInfoBuilder userType(String userType) {
            this.userType = UserTypeEnum.findByValue(userType);
            return this;
        }
    }
}

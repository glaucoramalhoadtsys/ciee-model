package br.com.adtsys.ciee.model.system.unit.unit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "O tipo de unidade não foi encontrado.")
public class InvalidUnitTypeException extends RuntimeException {
    private static final long serialVersionUID = 3738976933444247023L;
}

package br.com.adtsys.ciee.model.system.student.auth.enumeration;

public enum LoginTypeEnum {

    FACEBOOK("FACEBOOK"),
    KAIROS("KAIROS"),
    LDAP("LDAP"),
    GOOGLE_PLUS("GOOGLE_PLUS");

    private String type;

    private LoginTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}

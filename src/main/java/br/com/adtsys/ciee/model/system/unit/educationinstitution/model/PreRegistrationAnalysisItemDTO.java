package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PreRegistrationAnalysisItemDTO implements Serializable {
    private static final long serialVersionUID = -5512571129980054823L;

    private String item;
    private String status;
    private String comments;
}

package br.com.adtsys.ciee.model.system.auth.authentication.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

/**
 * Created by adtsys on 6/8/17.
 */
@ResponseStatus(value = UNAUTHORIZED, reason = "Senha Inválida!")
public class InvalidUserPassword extends RuntimeException {
    private static final long serialVersionUID = -6248654844597833629L;

    public InvalidUserPassword(final String msg) {
        super(msg);
    }
}

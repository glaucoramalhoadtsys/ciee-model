package br.com.adtsys.ciee.model.system.auth.session.data.repository;

import br.com.adtsys.ciee.model.system.auth.session.data.repository.model.Session;
import br.com.adtsys.ciee.model.system.auth.user.data.model.User;

import java.util.Optional;

public interface SessionRepository extends SessionEntityRepository<Session> {

    Optional<Session> findByUserAndActive(final User user, final Boolean active);

    Optional<Session> findByToken(final String token);

    void deleteAllByUser(final User user);

}

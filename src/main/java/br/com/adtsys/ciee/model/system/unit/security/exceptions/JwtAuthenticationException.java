package br.com.adtsys.ciee.model.system.unit.security.exceptions;

import org.springframework.security.core.AuthenticationException;

public class JwtAuthenticationException extends AuthenticationException {

    private static final long serialVersionUID = 5986425618579154787L;

    public JwtAuthenticationException(final String msg) {
        super(msg);
    }

}

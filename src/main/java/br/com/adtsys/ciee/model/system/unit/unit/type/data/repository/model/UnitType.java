package br.com.adtsys.ciee.model.system.unit.unit.type.data.repository.model;

import br.com.adtsys.ciee.model.system.unit.base.AbstractEntity;
import br.com.adtsys.ciee.model.system.unit.unit.data.repository.model.Unit;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;

@Builder
@Entity(name = "unidade_tipos")
@AttributeOverride(name = "id", column = @Column(name = "id"))
public class UnitType extends AbstractEntity {

    private static final long serialVersionUID = -5894653446441783872L;
    @Id
    @Column(name = "id")

    @GeneratedValue(generator = "custom-sequence", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(unique = true, name = "descricao", nullable = false, length = 100)
    private String description;

    @Tolerate
    public UnitType() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        final Unit unit = (Unit) o;

        return getId() != null ? getId().equals(unit.getId()) : unit.getId() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        return result;
    }
}

package br.com.adtsys.ciee.model.system.unit.unit.data.repository.model;

import br.com.adtsys.ciee.model.system.unit.base.AbstractEntity;
import br.com.adtsys.ciee.model.system.unit.unit.type.data.repository.model.UnitType;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Entity(name = "unidades")
public class Unit extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = -7700411483523650457L;
    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "id_unidade_tipo", nullable = false)
    private UnitType unitType;
    @Column(unique = true, name = "descricao", nullable = false, length = 150)
    private String description;
    @Column(unique = true, name = "id_dominio")
    private Long domainID;

    @Tolerate
    public Unit() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Long getDomainID() {
        return domainID;
    }

    public void setDomainID(Long domainID) {
        this.domainID = domainID;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;

        final Unit unit = (Unit) o;

        return getId() != null ? getId().equals(unit.getId()) : unit.getId() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        return result;
    }
}

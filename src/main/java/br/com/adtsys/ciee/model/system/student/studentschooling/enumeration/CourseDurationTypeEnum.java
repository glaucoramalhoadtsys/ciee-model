package br.com.adtsys.ciee.model.system.student.studentschooling.enumeration;

public enum CourseDurationTypeEnum {
    A("Anual"),
    S("Semestral"),
    M("Mensal");

    private final String value;

    private CourseDurationTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

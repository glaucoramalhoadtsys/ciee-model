package br.com.adtsys.ciee.model.system.auth.user.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(value = BAD_REQUEST)
public class CannotCreateUserException extends RuntimeException {

    private static final long serialVersionUID = -998821318017876085L;

    public CannotCreateUserException(final Throwable e) {
        super(e);
    }
}

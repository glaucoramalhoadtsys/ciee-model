package br.com.adtsys.ciee.model.system.student.student.model;

import br.com.adtsys.ciee.model.system.student.student.enumeration.StudentSituationEnum;

import java.time.LocalDate;

/**
 * Modelo que representa os parâmetros utilizados na busca de um Estudante
 */

public class StudentFiterDTO {

    private String name;
    private String cpf;

    private LocalDate birthDate;

    private String code;
    private String city;
    private String state;
    private String address;
    private String number;
    private String complement;
    private String zipCode;
    private String email;
    private String phone;
    private Boolean pcd;
    private StudentSituationEnum situation;
    private Boolean pcdPending;
    private Boolean pcdInAnalysis;
}

package br.com.adtsys.ciee.model.system.core.phone.enums;

public enum PhoneTypeEnum {
    CELLPHONE("CELULAR"),
    LANDLINE("FIXO");

    private String phoneType;

    PhoneTypeEnum(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getPhoneType() {
        return phoneType;
    }
}

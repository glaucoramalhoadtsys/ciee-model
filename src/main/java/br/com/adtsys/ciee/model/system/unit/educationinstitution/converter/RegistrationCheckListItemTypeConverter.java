package br.com.adtsys.ciee.model.system.unit.educationinstitution.converter;

import br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration.RegistrationCheckListItemType;

import javax.persistence.AttributeConverter;
import java.util.Objects;

public class RegistrationCheckListItemTypeConverter implements AttributeConverter<RegistrationCheckListItemType, String> {

    @Override
    public String convertToDatabaseColumn(final RegistrationCheckListItemType itemType) {
        if (Objects.nonNull(itemType)) {
            return itemType.getValue();
        }
        return null;
    }

    @Override
    public RegistrationCheckListItemType convertToEntityAttribute(final String column) {
        for (final RegistrationCheckListItemType itemType : RegistrationCheckListItemType.values()) {
            if (column.equals(itemType.getValue())) {
                return itemType;
            }
        }
        return null;
    }

}

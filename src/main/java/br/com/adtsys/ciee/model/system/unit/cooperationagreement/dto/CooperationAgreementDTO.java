package br.com.adtsys.ciee.model.system.unit.cooperationagreement.dto;

import br.com.adtsys.ciee.model.system.unit.address.data.repository.model.Address;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.model.EducationInstitution;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.model.EducationInstitutionDocument;
import br.com.adtsys.ciee.model.system.unit.email.data.repository.model.Email;
import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import br.com.adtsys.ciee.model.system.unit.phone.data.repository.model.Phone;
import br.com.adtsys.ciee.model.system.unit.wallet.data.repository.model.Wallet;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class CooperationAgreementDTO implements Serializable {

  private Long educationInstitutionId;

  private Wallet wallet;

  private String institutionName;

  private String shortInstitutionName;

  private String authorizationCode;

  private String educationDegree;

  private Long institutionType;

  private String tradingName;

  private String shortTradingName;

  private String initials;

  private String popularName;

  private String stateRegistration;

  private String cityRegistration;

  private String cnpj;

  private String maintainer;

  private String shortMaintainer;

  private Person director;

  private Person viceDirector;

  private Address address;

  private Email email;

  private Set<Phone> phones;
  
  private List<EducationInstitutionDocument> documents;

  private String site;

  private String shortSite;

  private Boolean shouldCreateCooperationAgreement;

  private String cooperationAgreementReason;
  
  private Long agreementCode;
  
  private Boolean active;
  
  private Boolean sendAgreementDraft;

  private String situation;
  
  @JsonProperty(access = Access.READ_ONLY)
  private Boolean ead;
  
  private EducationInstitution educationInstitution;


  public Long getEducationInstitutionId() {
    return educationInstitutionId;
  }

  public void setEducationInstitutionId(Long educationInstitutionId) {
    this.educationInstitutionId = educationInstitutionId;
  }

  public Wallet getWallet() {
    return wallet;
  }

  public void setWallet(Wallet wallet) {
    this.wallet = wallet;
  }

  public String getInstitutionName() {
    return institutionName;
  }

  public void setInstitutionName(String institutionName) {
    this.institutionName = institutionName;
  }

  public String getShortInstitutionName() {
    return shortInstitutionName;
  }

  public void setShortInstitutionName(String shortInstitutionName) {
    this.shortInstitutionName = shortInstitutionName;
  }

  public String getAuthorizationCode() {
    return authorizationCode;
  }

  public void setAuthorizationCode(String authorizationCode) {
    this.authorizationCode = authorizationCode;
  }

  public String getEducationDegree() {
    return educationDegree;
  }

  public void setEducationDegree(String educationDegree) {
    this.educationDegree = educationDegree;
  }

  public Long getInstitutionType() {
    return institutionType;
  }

  public void setInstitutionType(Long institutionType) {
    this.institutionType = institutionType;
  }

  public String getTradingName() {
    return tradingName;
  }

  public void setTradingName(String tradingName) {
    this.tradingName = tradingName;
  }

  public String getShortTradingName() {
    return shortTradingName;
  }

  public void setShortTradingName(String shortTradingName) {
    this.shortTradingName = shortTradingName;
  }

  public String getInitials() {
    return initials;
  }

  public void setInitials(String initials) {
    this.initials = initials;
  }

  public String getPopularName() {
    return popularName;
  }

  public void setPopularName(String popularName) {
    this.popularName = popularName;
  }

  public String getStateRegistration() {
    return stateRegistration;
  }

  public void setStateRegistration(String stateRegistration) {
    this.stateRegistration = stateRegistration;
  }

  public String getCityRegistration() {
    return cityRegistration;
  }

  public void setCityRegistration(String cityRegistration) {
    this.cityRegistration = cityRegistration;
  }

  public String getCnpj() {
    return cnpj;
  }

  public void setCnpj(String cnpj) {
    this.cnpj = cnpj;
  }

  public String getMaintainer() {
    return maintainer;
  }

  public void setMaintainer(String maintainer) {
    this.maintainer = maintainer;
  }

  public String getShortMaintainer() {
    return shortMaintainer;
  }

  public void setShortMaintainer(String shortMaintainer) {
    this.shortMaintainer = shortMaintainer;
  }

  public Person getDirector() {
    return director;
  }

  public void setDirector(Person director) {
    this.director = director;
  }

  public Person getViceDirector() {
    return viceDirector;
  }

  public void setViceDirector(Person viceDirector) {
    this.viceDirector = viceDirector;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public Email getEmail() {
    return email;
  }

  public void setEmail(Email email) {
    this.email = email;
  }

  public Set<Phone> getPhones() {
    return phones;
  }

  public void setPhones(Set<Phone> phones) {
    this.phones = phones;
  }

  public List<EducationInstitutionDocument> getDocuments() {
    return documents;
  }

  public void setDocuments(List<EducationInstitutionDocument> documents) {
    this.documents = documents;
  }

  public String getSite() {
    return site;
  }

  public void setSite(String site) {
    this.site = site;
  }

  public String getShortSite() {
    return shortSite;
  }

  public void setShortSite(String shortSite) {
    this.shortSite = shortSite;
  }

  public Boolean getShouldCreateCooperationAgreement() {
    return shouldCreateCooperationAgreement;
  }

  public void setShouldCreateCooperationAgreement(Boolean shouldCreateCooperationAgreement) {
    this.shouldCreateCooperationAgreement = shouldCreateCooperationAgreement;
  }

  public String getCooperationAgreementReason() {
    return cooperationAgreementReason;
  }

  public void setCooperationAgreementReason(String cooperationAgreementReason) {
    this.cooperationAgreementReason = cooperationAgreementReason;
  }

  public Long getAgreementCode() {
    return agreementCode;
  }

  public void setAgreementCode(Long agreementCode) {
    this.agreementCode = agreementCode;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Boolean getSendAgreementDraft() {
    return sendAgreementDraft;
  }

  public void setSendAgreementDraft(Boolean sendAgreementDraft) {
    this.sendAgreementDraft = sendAgreementDraft;
  }

  public String getSituation() {
    return situation;
  }

  public void setSituation(String situation) {
    this.situation = situation;
  }

  public Boolean getEad() {
    return ead;
  }

  public void setEad(Boolean ead) {
    this.ead = ead;
  }

  public EducationInstitution getEducationInstitution() {
    return educationInstitution;
  }

  public void setEducationInstitution(EducationInstitution educationInstitution) {
    this.educationInstitution = educationInstitution;
  }
}

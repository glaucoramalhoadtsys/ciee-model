package br.com.adtsys.ciee.model.system.unit.campus.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CampusCoursePeriodSearchDTO {

    private String campusName;
    private String campusAddress;
    private String courseName;
    private String coursePeriodType;
    private Integer courseDuration;
    private String courseDurationType;
    private String city;
    private String state;
    @NotNull(message = "Parâmetro 'ead' é obrigatório")
    private Boolean ead;
    private String educationDegree;


}

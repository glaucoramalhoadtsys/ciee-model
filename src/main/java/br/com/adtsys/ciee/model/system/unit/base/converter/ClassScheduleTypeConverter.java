package br.com.adtsys.ciee.model.system.unit.base.converter;

import br.com.adtsys.ciee.model.system.unit.base.enumeration.ClassScheduleTypeEnum;

import javax.persistence.AttributeConverter;
import java.util.Objects;

public class ClassScheduleTypeConverter implements AttributeConverter<ClassScheduleTypeEnum, String> {

    @Override
    public String convertToDatabaseColumn(final ClassScheduleTypeEnum classScheduleType) {
        if (Objects.nonNull(classScheduleType)) {
            return classScheduleType.toString();
        }
        return null;
    }

    @Override
    public ClassScheduleTypeEnum convertToEntityAttribute(final String column) {
        for (final ClassScheduleTypeEnum classScheduleType : ClassScheduleTypeEnum.values()) {
            if (column.equals(classScheduleType.toString())) {
                return classScheduleType;
            }
        }
        return null;
    }

}
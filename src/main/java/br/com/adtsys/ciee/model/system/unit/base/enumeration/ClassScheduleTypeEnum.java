package br.com.adtsys.ciee.model.system.unit.base.enumeration;

public enum ClassScheduleTypeEnum {
    F("Fixo"),
    V("Variável");

    private final String value;

    ClassScheduleTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

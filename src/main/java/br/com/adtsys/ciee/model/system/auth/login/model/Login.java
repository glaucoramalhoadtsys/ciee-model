package br.com.adtsys.ciee.model.system.auth.login.model;

import br.com.adtsys.ciee.model.system.auth.login.enumeration.LoginTypeEnum;
import br.com.adtsys.ciee.model.system.auth.user.data.model.User;

/**
 * Entidade que representa um Login de um usuário no sistema de autenticação.
 */
public class Login {

    private Long id;

    private User user;

    private String userLogin;

    private LoginTypeEnum type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public LoginTypeEnum getType() {
        return type;
    }

    public void setType(LoginTypeEnum type) {
        this.type = type;
    }
}

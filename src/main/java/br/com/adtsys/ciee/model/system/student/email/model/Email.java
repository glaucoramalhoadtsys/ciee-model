package br.com.adtsys.ciee.model.system.student.email.model;

import br.com.adtsys.ciee.model.system.student.student.model.StudentResource;

/**
 * Entidade que representa um E-mail.
 */

public class Email extends StudentResource {


    private Long id;

    private String email;

    private Boolean main;

    private Boolean verified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getMain() {
        return main;
    }

    public void setMain(Boolean main) {
        this.main = main;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
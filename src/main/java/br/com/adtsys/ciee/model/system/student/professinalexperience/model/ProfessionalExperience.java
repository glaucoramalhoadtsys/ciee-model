package br.com.adtsys.ciee.model.system.student.professinalexperience.model;

import br.com.adtsys.ciee.model.system.student.student.model.StudentResource;
import org.apache.commons.lang3.BooleanUtils;

/**
 * Entidade que representa uma Experiência Profissional.
 */

public class ProfessionalExperience extends StudentResource {


    private Long id;

    private String companyName;

    private String position;

    private String startDate;

    private String endDate;

    private String activities;

    private Boolean current;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isCurrent() {
        return BooleanUtils.isTrue(this.current);
    }
}

package br.com.adtsys.ciee.model.system.auth.domain.data.repository;


import br.com.adtsys.ciee.model.system.auth.domain.data.repository.model.Domain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DomainRepository extends JpaRepository<Domain, Long>, JpaSpecificationExecutor<Domain> {

    Optional<List<Domain>> findAllByParentDomain(final Domain Id);

    @Query(value = "SELECT * FROM DOMINIOS START WITH ID = :domainId CONNECT BY PRIOR ID = ID_DOMINIO_PAI", nativeQuery = true)
    List<Domain> findAllDomainsHierarchy(@Param("domainId") Long domainId);
}

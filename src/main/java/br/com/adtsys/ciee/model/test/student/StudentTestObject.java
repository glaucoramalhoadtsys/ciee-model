package br.com.adtsys.ciee.model.test.student;

import br.com.adtsys.ciee.model.system.auth.user.data.model.User;
import br.com.adtsys.ciee.model.system.core.educationdegree.data.repository.EducationDegrees;
import br.com.adtsys.ciee.model.system.student.student.model.Student;

public class StudentTestObject {

    private Student student;
    private EducationDegrees educationDegree;
    private User user;

    public StudentTestObject() {

    }

    public StudentTestObject(Student student, EducationDegrees educationDegree, User user) {
        this.student = student;
        this.educationDegree = educationDegree;
        this.user = user;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public EducationDegrees getEducationDegree() {
        return educationDegree;
    }

    public void setEducationDegree(EducationDegrees educationDegree) {
        this.educationDegree = educationDegree;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

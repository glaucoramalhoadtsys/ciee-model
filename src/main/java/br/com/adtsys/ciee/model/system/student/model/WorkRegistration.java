package br.com.adtsys.ciee.model.system.student.model;

/**
 * Entidade contém dados da CARTEIRA DE TRABALHO do estudante.
 */

public class WorkRegistration {


    private long id;

    private String workRegistrationNumber;

    private String serie;

    private String state;

    private String nis;

    public Long getId() {
        return this.id;
    }

    public void setId(Long idValue) {
        this.id = idValue;
    }

}

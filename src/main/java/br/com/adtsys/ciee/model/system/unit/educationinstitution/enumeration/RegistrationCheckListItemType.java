package br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration;

public enum RegistrationCheckListItemType {
    DEGREE("NIVEL_IE"),
    AUTHORIZATION_CODE("COD_AUTORIZACAO_FUNC"),
    AUTHORIZATION_DOC("DOC_AUTORIZACAO_FUNC"),
    CNPJ("CNPJ");

    private String value;

    private RegistrationCheckListItemType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

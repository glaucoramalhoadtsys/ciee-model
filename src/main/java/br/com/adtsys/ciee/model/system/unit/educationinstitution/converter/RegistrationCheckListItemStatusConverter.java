package br.com.adtsys.ciee.model.system.unit.educationinstitution.converter;

import br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration.RegistrationCheckListItemStatus;

import javax.persistence.AttributeConverter;
import java.util.Objects;

public class RegistrationCheckListItemStatusConverter implements AttributeConverter<RegistrationCheckListItemStatus, String> {

    @Override
    public String convertToDatabaseColumn(final RegistrationCheckListItemStatus status) {
        if (Objects.nonNull(status)) {
            return status.getValue();
        }
        return null;
    }

    @Override
    public RegistrationCheckListItemStatus convertToEntityAttribute(final String value) {
        return RegistrationCheckListItemStatus.findByValue(value);
    }
}

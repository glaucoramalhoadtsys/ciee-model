package br.com.adtsys.ciee.model.system.student.essay.model;

import br.com.adtsys.ciee.model.system.student.essaytheme.model.EssayTheme;
import br.com.adtsys.ciee.model.system.student.student.model.StudentResource;

import java.time.ZonedDateTime;

/**
 * Entidade que representa uma Redação.
 */

public class Essay extends StudentResource {


    private Long id;

    private EssayTheme essayTheme;

    private String content;

    private ZonedDateTime finishedDate;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

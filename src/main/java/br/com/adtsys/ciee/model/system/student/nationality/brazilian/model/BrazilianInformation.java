package br.com.adtsys.ciee.model.system.student.nationality.brazilian.model;

import br.com.adtsys.ciee.model.system.student.converter.LocalDateAttributeConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import javax.persistence.Convert;
import java.time.LocalDate;

public class BrazilianInformation {


    private Long id;

    private String birthplace;

    private String birthplaceState;

    private String rg;

    private String documentEmitter;

    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate emitterDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getBirthplaceState() {
        return birthplaceState;
    }

    public void setBirthplaceState(String birthplaceState) {
        this.birthplaceState = birthplaceState;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getDocumentEmitter() {
        return documentEmitter;
    }

    public void setDocumentEmitter(String documentEmitter) {
        this.documentEmitter = documentEmitter;
    }

    public LocalDate getEmitterDate() {
        return emitterDate;
    }

    public void setEmitterDate(LocalDate emitterDate) {
        this.emitterDate = emitterDate;
    }
}

package br.com.adtsys.ciee.model.system.unit.security.exceptions;


public class JwtBadSignatureException extends RuntimeException {
    public JwtBadSignatureException(final String message) {
        super(message);
    }
}

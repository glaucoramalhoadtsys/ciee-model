package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EICampusCourseCreateContactDTO implements Serializable {
    private static final long serialVersionUID = -6064751128308445832L;

    private Long contactID;
}

package br.com.adtsys.ciee.model.system.student.studentenem.model;

import br.com.adtsys.ciee.model.system.student.student.model.Student;

import java.math.BigDecimal;

/**
 * Entidade que contém dados do ENEM em complemento a escolaridade de um estudante
 */

public class StudentEnem {


    private Long id;

    private Student student;

    private Boolean attended;

    private Integer year;

    private BigDecimal testIScore;

    private BigDecimal testIIScore;

    private BigDecimal testIIIScore;

    private BigDecimal testIVScore;

    private BigDecimal testVScore;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

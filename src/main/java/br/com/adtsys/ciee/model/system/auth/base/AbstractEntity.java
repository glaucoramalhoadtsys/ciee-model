package br.com.adtsys.ciee.model.system.auth.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.ZonedDateTime;
import java.util.Objects;

@MappedSuperclass
@Data
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractEntity implements Persistable<Long> {

    private static final long serialVersionUID = 459598530790738402L;

    @Column(name = "data_criacao", nullable = false, updatable = false)
    @CreatedDate
    @JsonIgnore
    private ZonedDateTime creationDate;

    @Column(name = "data_alteracao", nullable = false)
    @LastModifiedDate
    @JsonIgnore
    private ZonedDateTime modifiedDate;

    @Column(name = "modificado_por")
    @LastModifiedBy
    @JsonIgnore
    private String modifiedBy;

    @Column(name = "criado_por")
    @CreatedBy
    @JsonIgnore
    private String createdBy;

    @Column(name = "deletado", nullable = false)
    @JsonIgnore
    private Boolean deleted = false;

    @Override
    public abstract Long getId();

    @Override
    @JsonIgnore
    public boolean isNew() {
        return Objects.isNull(this.getId());
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getModifiedDate() {
        return this.modifiedDate;
    }

    public void setModifiedDate(final ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(final String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(final String createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean isDeleted() {
        return this.deleted;
    }

    public void setDeleted(final Boolean deleted) {
        this.deleted = deleted;
    }
}

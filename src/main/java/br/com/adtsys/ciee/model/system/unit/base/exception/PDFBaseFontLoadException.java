package br.com.adtsys.ciee.model.system.unit.base.exception;

public class PDFBaseFontLoadException extends RuntimeException {
    private static final long serialVersionUID = 6904664306250221453L;

    public PDFBaseFontLoadException(String msg) {
        super(msg);
    }
}

package br.com.adtsys.ciee.model.system.student.enrollmentpermissions.model;

public class EnrollmentPermissionDTO {

    private String state;
    private String educationDegree;
    private Boolean allowed;
    private String link;

}

package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PreRegistrationAnalysisDTO implements Serializable {
    private static final long serialVersionUID = -5683709347381700583L;

    private List<PreRegistrationAnalysisItemDTO> items;
}

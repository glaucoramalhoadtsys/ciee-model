package br.com.adtsys.ciee.model.system.auth.authentication.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@ResponseStatus(value = UNAUTHORIZED, reason = "Não foi encontrado perfil de acesso do Usuário. Solicite a atualização do seu perfil.")
public class InvalidUserNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 5683606912888583463L;

    public InvalidUserNotFoundException() {
        super();
    }

    public InvalidUserNotFoundException(final String message) {
        super(message);
    }

}


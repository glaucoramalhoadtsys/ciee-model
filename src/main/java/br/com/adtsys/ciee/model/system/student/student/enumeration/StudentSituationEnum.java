package br.com.adtsys.ciee.model.system.student.student.enumeration;

public enum StudentSituationEnum {

    ACTIVE("ATIVO"),
    BLOCKED("BLOQUEADO"),
    INACTIVE("INATIVO");

    private String situation;

    private StudentSituationEnum(String situation) {
        this.situation = situation;
    }

    public String getSituation() {
        return situation;
    }

}

package br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration;

public enum EIStatus {

    PENDENTE("PENDENTE"), REPROVADO(
            "REPROVADO"), ATIVO("ATIVO"), INATIVO(
            "INATIVO");


    private final String status;

    private EIStatus(final String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

}

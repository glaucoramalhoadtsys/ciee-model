package br.com.adtsys.ciee.model.system.core.coverage.data.repository.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by daniel on 30/06/17.
 */
@Builder
@Entity(name = "abrangencias")
@AllArgsConstructor
public class Coverage extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 6807443879363986580L;
    @Id
    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Size(message = "'Tipo de Abrangência' deve se limitar a 1 caractere", max = 1, min = 1)

    @Column(name = "tipo_abrangencia", nullable = false, length = 1)
    private String coverageType;
    @Size(message = "'Descrição da Abrangência' possui limite máximo de 25 caracteres", max = 25)

    @Column(name = "descricao_abrangencia", nullable = false)
    private String coverageDescription;

    @Tolerate
    public Coverage() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getCoverageType() {
        return this.coverageType;
    }

    public void setCoverageType(final String coverageType) {
        this.coverageType = coverageType;
    }

    public String getCoverageDescription() {
        return this.coverageDescription;
    }

    public void setCoverageDescription(final String coverageDescription) {
        this.coverageDescription = coverageDescription;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        final Coverage coverage = (Coverage) o;

        return this.id.equals(coverage.id);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + this.id.hashCode();
        return result;
    }
}

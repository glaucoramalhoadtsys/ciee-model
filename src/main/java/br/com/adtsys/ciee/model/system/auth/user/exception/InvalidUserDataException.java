package br.com.adtsys.ciee.model.system.auth.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidUserDataException extends RuntimeException {

    private static final long serialVersionUID = -2797230254442538842L;

    public InvalidUserDataException(String message) {
        super(message);
    }
}

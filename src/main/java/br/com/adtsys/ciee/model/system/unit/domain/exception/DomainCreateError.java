package br.com.adtsys.ciee.model.system.unit.domain.exception;

public class DomainCreateError extends RuntimeException {
    private static final long serialVersionUID = -2448467288669660294L;

    public DomainCreateError(String msg) {
        super(msg);
    }
}

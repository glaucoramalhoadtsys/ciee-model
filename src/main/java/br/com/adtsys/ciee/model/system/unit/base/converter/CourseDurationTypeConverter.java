package br.com.adtsys.ciee.model.system.unit.base.converter;

import br.com.adtsys.ciee.model.system.unit.base.enumeration.CourseDurationTypeEnum;

import javax.persistence.AttributeConverter;
import java.util.Objects;

public class CourseDurationTypeConverter implements AttributeConverter<CourseDurationTypeEnum, String> {

    @Override
    public String convertToDatabaseColumn(final CourseDurationTypeEnum durationType) {
        if (Objects.nonNull(durationType)) {
            return durationType.toString();
        }
        return null;
    }

    @Override
    public CourseDurationTypeEnum convertToEntityAttribute(final String column) {
        for (final CourseDurationTypeEnum durationType : CourseDurationTypeEnum.values()) {
            if (column.equals(durationType.toString()) || column.equals(durationType.getValue())) {
                return durationType;
            }
        }
        return null;
    }

}
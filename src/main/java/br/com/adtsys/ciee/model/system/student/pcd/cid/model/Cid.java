package br.com.adtsys.ciee.model.system.student.pcd.cid.model;


import br.com.adtsys.ciee.model.system.student.student.model.StudentResource;

/**
 * Entidade que representa CIDs
 */

public class Cid extends StudentResource {


    private Long id;

    private String code;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

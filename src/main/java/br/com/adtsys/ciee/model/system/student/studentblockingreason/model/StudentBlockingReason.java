package br.com.adtsys.ciee.model.system.student.studentblockingreason.model;

import br.com.adtsys.ciee.model.system.student.blockingreason.model.BlockingReason;
import br.com.adtsys.ciee.model.system.student.student.model.Student;

public class StudentBlockingReason {


    private Long id;

    private Student student;

    private BlockingReason blockingReason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

package br.com.adtsys.ciee.model.system.unit.management.data.repository.model;

import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import br.com.adtsys.ciee.model.system.unit.superintendence.data.repository.model.Superintendence;
import br.com.adtsys.ciee.model.system.unit.unit.data.repository.model.Unit;


public class Management {

  public Management() {}

  private Long id;

  private Unit unit;

  private String description;

  private String reducedDescription;

  private String initials;

  private Person responsible;

  private Superintendence superintendence;

  private Boolean active;
}

package br.com.adtsys.ciee.model.system.unit.email.exception;

import br.com.adtsys.ciee.model.system.unit.base.exception.ClientException;

public class MailerServiceClientException extends ClientException {

    private static final long serialVersionUID = 1L;

    public MailerServiceClientException(String message) {
        super(message);
    }

}

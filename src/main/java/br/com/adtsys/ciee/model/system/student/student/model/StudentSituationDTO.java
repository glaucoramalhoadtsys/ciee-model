package br.com.adtsys.ciee.model.system.student.student.model;

import br.com.adtsys.ciee.model.system.student.student.enumeration.StudentSituationEnum;
import br.com.adtsys.ciee.model.system.student.studentblockingreason.model.StudentBlockingReasonDTO;

import java.util.List;

public class StudentSituationDTO {

    private StudentSituationEnum situation;

    private List<StudentBlockingReasonDTO> blockingReasons;

}
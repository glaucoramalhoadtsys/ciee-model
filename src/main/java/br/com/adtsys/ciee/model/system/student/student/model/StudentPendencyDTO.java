package br.com.adtsys.ciee.model.system.student.student.model;

import br.com.adtsys.ciee.model.system.student.student.enumeration.StudentPendencyEnum;

/**
 * Modelo que representa informações sobre pendências do estudante autenticado
 */

public class StudentPendencyDTO {

    private StudentPendencyEnum pendency;

}

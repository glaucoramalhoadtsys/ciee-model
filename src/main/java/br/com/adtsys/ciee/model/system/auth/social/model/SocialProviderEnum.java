package br.com.adtsys.ciee.model.system.auth.social.model;

public enum SocialProviderEnum {
    FACEBOOK("facebook"),
    GOOGLE("google");

    private String value;

    private SocialProviderEnum(String value) {
        this.value = value;
    }

    public static SocialProviderEnum findByValue(String value) {
        for (final SocialProviderEnum socialProviderEnum : values()) {
            if (value.equalsIgnoreCase(socialProviderEnum.getValue())) {
                return socialProviderEnum;
            }
        }

        return null;
    }

    public String getValue() {
        return value;
    }
}

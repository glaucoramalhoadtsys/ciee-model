package br.com.adtsys.ciee.model.system.student.language.model;

public enum Level {
    BASIC("BÁSICO"),
    INTERMEDIATE("INTERMEDIÁRIO"),
    ADVANCED("AVANÇADO");

    private String translate;

    private Level(String translate) {
        this.translate = translate;
    }

    public String getTranslate() {
        return this.translate;
    }

}

package br.com.adtsys.ciee.model.system.auth.session.data.repository.model;

import br.com.adtsys.ciee.model.system.auth.base.AbstractEntity;
import br.com.adtsys.ciee.model.system.auth.user.data.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "sessoes")
@Builder
@AllArgsConstructor
public class Session extends AbstractEntity {

    private static final long serialVersionUID = -4919281903007226886L;
    @Id

    @GeneratedValue(generator = "custom-sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private User user;
    @Column(name = "token", nullable = false, length = 10000)

    private String token;
    @Column(name = "ativo", nullable = false)
    @NotNull(message = "'Ativo' deve ser preenchido")
    private Boolean active;

    @Tolerate
    public Session() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public Boolean isActive() {
        return this.active;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }
}

package br.com.adtsys.ciee.model.system.unit.security.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ErrorMessage {

    private String message;
    private String code;
    private String detail;

    public ErrorMessage(final String message, final String code) {
        this(message, code, "");
    }

    public ErrorMessage(final String message) {
        this(message, "", "");
    }

}

package br.com.adtsys.ciee.model.system.unit.storage.exception;

public class StorageException extends RuntimeException {

    private static final long serialVersionUID = 4420242867154611286L;

    public StorageException(final String message) {
        super(message);
    }

    public StorageException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

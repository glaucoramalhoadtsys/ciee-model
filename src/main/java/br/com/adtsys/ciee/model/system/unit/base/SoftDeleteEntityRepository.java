package br.com.adtsys.ciee.model.system.unit.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@NoRepositoryBean
public interface SoftDeleteEntityRepository<T extends AbstractEntity>
        extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {

    @Override
    default List<T> findAll() {
        return this.findByDeleted(false);
    }

    // @Override
    default T findOne(Long id) {
        return this.findByIdAndDeleted(id, false);
    }

    // @Override
    default void delete(Iterable<? extends T> entities) {
        entities.forEach(entitiy -> delete(entitiy.getId()));
    }

    // @Override
    default boolean exists(Long id) {
        return findOne(id) != null;
    }

    // @Override
    default void delete(Long id) {
        this.delete(this.findOne(id));
    }

    @Override
    default void delete(T entity) {
        entity.setDeleted(true);
        this.save(entity);
    }

    @RestResource(exported = false)
    public T findByIdAndDeleted(Long id, Boolean deleted);

    @RestResource(exported = false)
    public List<T> findByDeleted(Boolean deleted);

}

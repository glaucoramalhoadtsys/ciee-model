package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import br.com.adtsys.ciee.model.system.unit.address.data.repository.model.Address;
import br.com.adtsys.ciee.model.system.unit.campus.model.Campus;
import br.com.adtsys.ciee.model.system.unit.email.data.repository.model.Email;
import br.com.adtsys.ciee.model.system.unit.institutiontype.repository.model.InstitutionType;
import br.com.adtsys.ciee.model.system.unit.phone.data.repository.model.Phone;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Set;

public class EducationInstitution {
    private Long id;
    private String cnpj;
    private List<Campus> campus;

    private String shortInstitutionName;
    private String tradingName;
    private String shortTradingName;
    private String popularName;
    private String initials;

    private String stateRegistration;
    private String cityRegistration;
    private String maintainer;
    private String shortMaintainer;
    private String site;
    private String shortSite;
    private Email email;

    private Set<Phone> phones;
    private Boolean ead = false;

    private String institutionName;
    private String educationDegree;
    private String course;
    private String authorizationCode;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private InstitutionType institutionType;
    private Boolean active;
    private Long cieeCode;
    private Set<EIContactType> eiContactTypes;
    private Long unitID;
    private Long domainId;
    private Address address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public List<Campus> getCampus() {
        return campus;
    }

    public void setCampus(List<Campus> campus) {
        this.campus = campus;
    }

    public String getShortInstitutionName() {
        return shortInstitutionName;
    }

    public void setShortInstitutionName(String shortInstitutionName) {
        this.shortInstitutionName = shortInstitutionName;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getShortTradingName() {
        return shortTradingName;
    }

    public void setShortTradingName(String shortTradingName) {
        this.shortTradingName = shortTradingName;
    }

    public String getPopularName() {
        return popularName;
    }

    public void setPopularName(String popularName) {
        this.popularName = popularName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getStateRegistration() {
        return stateRegistration;
    }

    public void setStateRegistration(String stateRegistration) {
        this.stateRegistration = stateRegistration;
    }

    public String getCityRegistration() {
        return cityRegistration;
    }

    public void setCityRegistration(String cityRegistration) {
        this.cityRegistration = cityRegistration;
    }

    public String getMaintainer() {
        return maintainer;
    }

    public void setMaintainer(String maintainer) {
        this.maintainer = maintainer;
    }

    public String getShortMaintainer() {
        return shortMaintainer;
    }

    public void setShortMaintainer(String shortMaintainer) {
        this.shortMaintainer = shortMaintainer;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getShortSite() {
        return shortSite;
    }

    public void setShortSite(String shortSite) {
        this.shortSite = shortSite;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    public Boolean getEad() {
        return ead;
    }

    public void setEad(Boolean ead) {
        this.ead = ead;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getEducationDegree() {
        return educationDegree;
    }

    public void setEducationDegree(String educationDegree) {
        this.educationDegree = educationDegree;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public InstitutionType getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(InstitutionType institutionType) {
        this.institutionType = institutionType;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getCieeCode() {
        return cieeCode;
    }

    public void setCieeCode(Long cieeCode) {
        this.cieeCode = cieeCode;
    }

    public Set<EIContactType> getEiContactTypes() {
        return eiContactTypes;
    }

    public void setEiContactTypes(Set<EIContactType> eiContactTypes) {
        this.eiContactTypes = eiContactTypes;
    }

    public Long getUnitID() {
        return unitID;
    }

    public void setUnitID(Long unitID) {
        this.unitID = unitID;
    }

    public Long getDomainId() {
        return domainId;
    }

    public void setDomainId(Long domainId) {
        this.domainId = domainId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
package br.com.adtsys.ciee.model.system.auth.social.model;

import lombok.Builder;
import lombok.Data;

/**
 * Modelo que representa um usuário logado via Rede Social.
 */
@Data
@Builder
public final class SocialUser {

    private String id;
    private String firstName;
    private String lastName;
    private String photoUrl;
    private String email;

}

package br.com.adtsys.ciee.model.test.course;

public enum BasicLinked {

    BASICO("BÁSICO"),
    ASSOCIADO("ASSOCIADO");

    final String basicLinked;

    BasicLinked(String basicLinked) {
        this.basicLinked = basicLinked;
    }

    public String getBasicLinked() {
        return basicLinked;
    }
}

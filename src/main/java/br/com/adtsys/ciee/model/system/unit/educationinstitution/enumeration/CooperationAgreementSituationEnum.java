package br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration;

public enum CooperationAgreementSituationEnum {

    PENDENTE("PENDENTE"),
    ENVIADO_PARA_ANALISE("ENVIADO PARA ANALISE"),
    APROVADO("APROVADO"),
    REPROVADO("REPROVADO");

    private final String status;

    private CooperationAgreementSituationEnum(final String status) {
        this.status = status;
    }

    public static CooperationAgreementSituationEnum findByStatus(final String status) {
        for (CooperationAgreementSituationEnum s : values()) {
            if (s.getStatus().equals(status)) {
                return s;
            }
        }
        return null;
    }

    public String getStatus() {
        return this.status;
    }

}

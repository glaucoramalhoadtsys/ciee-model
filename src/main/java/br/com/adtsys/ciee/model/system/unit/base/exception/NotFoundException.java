package br.com.adtsys.ciee.model.system.unit.base.exception;

public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = -147646869234159977L;

    public NotFoundException(String msg) {
        super(msg);
    }

    public NotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }
}

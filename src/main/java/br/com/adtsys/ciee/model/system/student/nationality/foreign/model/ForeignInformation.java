package br.com.adtsys.ciee.model.system.student.nationality.foreign.model;

import java.time.LocalDate;

public class ForeignInformation {


    private Long id;

    private String rne;

    private LocalDate permanencyDate;

    private Long visaClassification;

    private LocalDate arrivalDate;

    private Boolean marriedWithBrazilian = false;

    private Boolean brazilianChildren = false;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

package br.com.adtsys.ciee.model.system.core.atuationarea.data.repository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Builder
@Entity(name = "tipos_areas_atuacao")
@AllArgsConstructor
public class AtuationAreaType implements Serializable {

    private static final long serialVersionUID = 8564041231243828140L;
    @Id
    @Column(name = "sigla", nullable = false, length = 1)
    private String initials;
    @Column(name = "descricao")
    private String description;


    @Tolerate
    public AtuationAreaType() {
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.initials == null) ? 0 : this.initials.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final AtuationAreaType other = (AtuationAreaType) obj;
        if (this.initials == null) {
            if (other.initials != null) return false;
        } else if (!this.initials.equals(other.initials)) return false;
        return true;
    }

}

package br.com.adtsys.ciee.model.system.unit.campus.model;


import br.com.adtsys.ciee.model.system.unit.address.data.repository.model.Address;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.model.EducationInstitution;
import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class Campus {

    private Long id;


    private EducationInstitution educationInstitution;

    private String name;

    private String fantasyName;

    private String cnpj;

    private Address address;

    private List<Person> contacts;

    private Person principal;

    private Boolean pronatec = Boolean.FALSE;


    private Boolean active = Boolean.TRUE;


    private List<CampusCourse> campusCourses;


    private Long cieeCode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public EducationInstitution getEducationInstitution() {
        return educationInstitution;
    }

    public void setEducationInstitution(EducationInstitution educationInstitution) {
        this.educationInstitution = educationInstitution;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFantasyName() {
        return fantasyName;
    }

    public void setFantasyName(String fantasyName) {
        this.fantasyName = fantasyName;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Person> getContacts() {
        return contacts;
    }

    public void setContacts(List<Person> contacts) {
        this.contacts = contacts;
    }

    public Person getPrincipal() {
        return principal;
    }

    public void setPrincipal(Person principal) {
        this.principal = principal;
    }

    public Boolean getPronatec() {
        return pronatec;
    }

    public void setPronatec(Boolean pronatec) {
        this.pronatec = pronatec;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<CampusCourse> getCampusCourses() {
        return campusCourses;
    }

    public void setCampusCourses(List<CampusCourse> campusCourses) {
        this.campusCourses = campusCourses;
    }

    public Long getCieeCode() {
        return cieeCode;
    }

    public void setCieeCode(Long cieeCode) {
        this.cieeCode = cieeCode;
    }
}

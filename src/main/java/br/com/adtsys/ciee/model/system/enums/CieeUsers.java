package br.com.adtsys.ciee.model.system.enums;

public enum CieeUsers {
    ADMIN_SISTEMAS("Admin Sistemas"),
    ADMIN_BACKOFFICE("Admin BackOffice"),
    BACKOFFICE_ESCOLA("Backoffice Escola"),
    BACKOFFICE_APRENDIZ("Backoffice Aprendiz"),
    CONTROLE_OPERACIONAL("Controle Operacional"),
    NÚCLEO("Backoffice Núcleo"),
    GERENCIADOR_EMAIL("Gerenciador Email"),
    BACKOFFICE_GESTOR_ESCOLA("Backoffice Gestor Escola"),
    BACKOFFICE_ESTUDANTE("Backoffice Estudante"),
    BACKOFFICE_GESTOR_ESTUDANTE("Backoffice Gestor Estudante"),
    BACKOFFICE_GESTOR_ESCOLA_RJ("Backoffice Gestor Escola RJ"),
    BACKOFFICE_ESTUDANTE_PCD("Backoffice Estudante PCD"),
    BACKOFFICE_ASSISTENTE_SOCIAL("Backoffice Assistente Social"),
    ADMIN_CONTRATOS("Admin Contratos"),
    CONSULTOR("Consultor"),
    APOIO_CONSULTOR("Apoio Consultor"),
    CTO("CTO"),
    CONTAS_A_RECEBER("Contas a Receber"),
    BACKOFFICE_CIEE("Backoffice CIEE"),
    RH("RH"),
    BACKOFFICE_SECOR("BackOffice Secor"),
    BACKOFFICE_ADMINISTRACAO_DE_CONTRATOS("Backoffice Administracao de Contratos");

    private String description;

    CieeUsers(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

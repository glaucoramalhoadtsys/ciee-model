package br.com.adtsys.ciee.model.system.unit.base.enumeration;

public enum CourseModalityEnum {
    P("Presencial"),
    E("EAD");

    private String value;

    CourseModalityEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

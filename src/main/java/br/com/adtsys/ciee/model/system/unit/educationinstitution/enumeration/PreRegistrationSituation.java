package br.com.adtsys.ciee.model.system.unit.educationinstitution.enumeration;


public enum PreRegistrationSituation {

    AGUARDANDO_DOCUMENTACAO("AGUARDANDO DOCUMENTACAO"),
    ENVIADO_PARA_ANALISE("ENVIADO PARA ANALISE"),
    APROVADO("APROVADO"),
    REPROVADO("REPROVADO"),
    PENDENTE_REENVIO_DOCUMENTACAO("PENDENTE REENVIO DE DOCUMENTACAO");


    private final String status;

    private PreRegistrationSituation(final String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

}

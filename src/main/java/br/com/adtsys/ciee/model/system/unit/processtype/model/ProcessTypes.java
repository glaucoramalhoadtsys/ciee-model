package br.com.adtsys.ciee.model.system.unit.processtype.model;

public enum ProcessTypes {
    ESCOLA,
    EMPRESA,
    ESTUDANTE,
    CONTRATO,
    FINANCEIRO,
    SELEÇÃO;
}
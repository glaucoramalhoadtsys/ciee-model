package br.com.adtsys.ciee.model.system.unit.base;

import br.com.adtsys.ciee.model.system.unit.base.enumeration.UnitTypeIdentifier;

public interface UnitEntity {

    UnitTypeIdentifier unitTypeIdentifier();

    String unitDescription();

}

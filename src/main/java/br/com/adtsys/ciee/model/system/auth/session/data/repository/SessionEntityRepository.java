package br.com.adtsys.ciee.model.system.auth.session.data.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface SessionEntityRepository<T>
        extends
        CrudRepository<T, Long>,
        JpaSpecificationExecutor<T> {
}

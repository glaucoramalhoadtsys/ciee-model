package br.com.adtsys.ciee.model.system.auth.user.exception;

import br.com.adtsys.ciee.model.system.auth.base.exception.NotFoundException;

public class UserNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 1L;

    public UserNotFoundException() {
        super("Usuário não encontrado");
    }

    public UserNotFoundException(String message) {
        super(message);
    }

}

package br.com.adtsys.ciee.model.system.unit.processtype.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tipos_de_processo")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProcessType {

    @Id
    private Long id;

    @Size(message = "'Descrição' deve ter tamanho máximo de 35 caracteres", max = 35)
    @Column(name = "descricao", nullable = false, length = 35)
    private String description;

//  @OneToMany(mappedBy = "processType")
//  private Set<EIContactType> eiContactTypes;
}

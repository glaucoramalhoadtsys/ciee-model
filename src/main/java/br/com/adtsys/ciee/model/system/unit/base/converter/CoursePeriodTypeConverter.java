package br.com.adtsys.ciee.model.system.unit.base.converter;

import br.com.adtsys.ciee.model.system.unit.base.enumeration.CoursePeriodTypeEnum;

import javax.persistence.AttributeConverter;
import java.util.Objects;

public class CoursePeriodTypeConverter implements AttributeConverter<CoursePeriodTypeEnum, String> {

    @Override
    public String convertToDatabaseColumn(final CoursePeriodTypeEnum periodType) {
        if (Objects.nonNull(periodType)) {
            return periodType.toString();
        }
        return null;
    }

    @Override
    public CoursePeriodTypeEnum convertToEntityAttribute(final String column) {
        for (final CoursePeriodTypeEnum periodType : CoursePeriodTypeEnum.values()) {
            if (column.equals(periodType.toString()) || column.equals(periodType.getValue())) {
                return periodType;
            }
        }
        return null;
    }

}
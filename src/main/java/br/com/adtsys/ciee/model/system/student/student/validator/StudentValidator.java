package br.com.adtsys.ciee.model.system.student.student.validator;


import br.com.adtsys.ciee.model.system.student.student.model.Student;

/**
 * Interface para implementação de regras de negócio específicas de estudante usando design pattern Strategy
 */
public interface StudentValidator {

    void validate(Student student) throws Exception;

}

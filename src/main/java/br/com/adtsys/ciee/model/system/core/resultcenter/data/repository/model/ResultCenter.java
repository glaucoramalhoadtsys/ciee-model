package br.com.adtsys.ciee.model.system.core.resultcenter.data.repository.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Builder
@Entity(name = "centros_de_resultado")
@AllArgsConstructor
public class ResultCenter extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = -3651960679203270027L;
    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "codigo", unique = true)
    private Integer code;
    @Size(message = "'Descrição' deve ter no máximo 300 caracteres", max = 300)
    @Column(name = "descricao", nullable = false)
    private String description;
    @Column(name = "tipo_atividade", nullable = false)
    private Integer activityType;


    @Tolerate
    public ResultCenter() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.code == null) ? 0 : this.code.hashCode());
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final ResultCenter other = (ResultCenter) obj;
        if (this.code == null) {
            if (other.code != null) return false;
        } else if (!this.code.equals(other.code)) return false;
        if (this.id == null) {
            if (other.id != null) return false;
        } else if (!this.id.equals(other.id)) return false;
        return true;
    }
}

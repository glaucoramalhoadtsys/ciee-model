package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class CnpjCountRequest {
    private String cnpj;
    private Long count;
}

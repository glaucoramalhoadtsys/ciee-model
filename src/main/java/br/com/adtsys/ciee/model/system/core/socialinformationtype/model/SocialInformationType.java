package br.com.adtsys.ciee.model.system.core.socialinformationtype.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Builder
@Entity(name = "tipo_informacoes_sociais")
@AttributeOverride(name = "id", column = @Column(name = "id"))
public class SocialInformationType extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4446513470867971253L;
    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Size(message = "'Identificador' do Tipo de Programa Social deve ser uma letra", min = 1, max = 1)

    @Column(name = "sigla_programa_social", nullable = false, length = 1, unique = true)
    private Character socialProgramTypeInitial;
    @Size(message = "Descrição do Tipo de Programa Social possui limite máximo de 25 caracteres", max = 25)
    @Column(name = "descricao_tipo_programa_social", nullable = false, length = 25)
    private String description;


    @Tolerate
    public SocialInformationType() {
    }

    public Character getSocialProgramTypeInitial() {
        return socialProgramTypeInitial;
    }

    public void setSocialProgramTypeInitial(Character socialProgramTypeInitial) {
        this.socialProgramTypeInitial = socialProgramTypeInitial;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        final SocialInformationType socialInformationType = (SocialInformationType) o;

        return this.socialProgramTypeInitial != null ? this.socialProgramTypeInitial.equals(socialInformationType.socialProgramTypeInitial) : socialInformationType.socialProgramTypeInitial == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.socialProgramTypeInitial != null ? this.socialProgramTypeInitial.hashCode() : 0);
        return result;
    }

}

package br.com.adtsys.ciee.model.system.unit.email.data.repository.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;


@Data
@Builder
public class SendEmailTemplateDTO {
    private List<String> toAddresses;

    private String templateIdentifier;

    private Map<String, String> templateParams;

}	

package br.com.adtsys.ciee.model.system.student.student.enumeration;

public enum DataValidationStatus {

    PENDENTE("PENDENTE"),
    IRREGULAR("IRREGULAR"),
    REGULAR("REGULAR"),
    DIVERGENTE("DIVERGENTE");

    private String status;

    private DataValidationStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}

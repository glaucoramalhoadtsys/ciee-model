package br.com.adtsys.ciee.model.system.core.internshipprogramparameter.data.repository.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by daniel on 14/07/17.
 */

@Builder
@Entity(name = "parametros_programa_estagio")
public class InternshipProgramParameter extends AbstractEntity {

    private static final long serialVersionUID = 3242119757502482924L;

    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull(message = "'Código do Ciee' deve ser preenchido.")
    @Column(name = "codigo_ciee", nullable = false, unique = true)
    private Long cieeCode;

    @NotNull(message = "'Jornada Diária Máxima' deve ser preenchido.")
    @Column(name = "jornada_maxima_diaria", nullable = false)
    private Short maximumDailyWork;

    @NotNull(message = "'Jornada Semanal Máxima' deve ser preenchido.")
    @Column(name = "jornada_maxima_semanal", nullable = false)
    private Short maximumWorkweek;

    @NotNull(message = "'Quantidade Mínima de Atividades' deve ser preenchido.")
    @Column(name = "quantidade_minima_atividades", nullable = false)
    private Short minimumActivitiesQuantity;

    @NotNull(message = "'Jornada Diária Máxima Educação Especial' deve ser preenchido.")
    @Column(name = "jornada_diaria_max_ed_especial", nullable = false)
    private Short maximumSpecialEducationDailyWork;

    @NotNull(message = "'Jornada Diária Máxima Educação Especial' deve ser preenchido.")
    @Column(name = "jornada_sem_max_ed_especial", nullable = false)
    private Short maximumSpecialEducationWorkweek;

    @Tolerate
    public InternshipProgramParameter() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getCieeCode() {
        return this.cieeCode;
    }

    public void setCieeCode(final Long cieeCode) {
        this.cieeCode = cieeCode;
    }

    public Short getMaximumDailyWork() {
        return this.maximumDailyWork;
    }

    public void setMaximumDailyWork(final Short maximumDailyWork) {
        this.maximumDailyWork = maximumDailyWork;
    }

    public Short getMaximumWorkweek() {
        return this.maximumWorkweek;
    }

    public void setMaximumWorkweek(final Short maximumWorkweek) {
        this.maximumWorkweek = maximumWorkweek;
    }

    public Short getMinimumActivitiesQuantity() {
        return this.minimumActivitiesQuantity;
    }

    public void setMinimumActivitiesQuantity(final Short minimumActivitiesQuantity) {
        this.minimumActivitiesQuantity = minimumActivitiesQuantity;
    }

    public Short getMaximumSpecialEducationDailyWork() {
        return this.maximumSpecialEducationDailyWork;
    }

    public void setMaximumSpecialEducationDailyWork(final Short maximumSpecialEducationDailyWork) {
        this.maximumSpecialEducationDailyWork = maximumSpecialEducationDailyWork;
    }

    public Short getMaximumSpecialEducationWorkweek() {
        return this.maximumSpecialEducationWorkweek;
    }

    public void setMaximumSpecialEducationWorkweek(final Short maximumSpecialEducationWorkweek) {
        this.maximumSpecialEducationWorkweek = maximumSpecialEducationWorkweek;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.cieeCode == null) ? 0 : this.cieeCode.hashCode());
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final InternshipProgramParameter other = (InternshipProgramParameter) obj;
        if (this.cieeCode == null) {
            if (other.cieeCode != null) return false;
        } else if (!this.cieeCode.equals(other.cieeCode)) return false;
        if (this.id == null) {
            if (other.id != null) return false;
        } else if (!this.id.equals(other.id)) return false;
        return true;
    }

}

package br.com.adtsys.ciee.model.system.core.usagelocation.data.repository.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Builder
@Entity(name = "locais_uso")
@AllArgsConstructor
public class UsageLocation extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = -9172376296843539342L;

    @Id

    @GeneratedValue(generator = "custom-generator", strategy = GenerationType.SEQUENCE)
    private Long id;


    @Size(message = "'Sigla' possui limite de 1 caractere", max = 1, min = 1)
    @Column(name = "sigla", length = 1, unique = true)
    private Character initials;


    @Size(message = "'Descrição' possui tamanho máximo de 20 caracteres", max = 35)
    @Column(name = "descricao", length = 35)
    private String description;

    @Tolerate
    public UsageLocation() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Character getInitials() {
        return this.initials;
    }

    public void setInitials(final Character initials) {
        this.initials = initials;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        result = prime * result + ((this.initials == null) ? 0 : this.initials.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final UsageLocation other = (UsageLocation) obj;
        if (this.id == null) {
            if (other.id != null) return false;
        } else if (!this.id.equals(other.id)) return false;
        if (this.initials == null) {
            if (other.initials != null) return false;
        } else if (!this.initials.equals(other.initials)) return false;
        return true;
    }

}

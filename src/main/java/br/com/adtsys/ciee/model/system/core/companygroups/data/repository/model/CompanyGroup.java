package br.com.adtsys.ciee.model.system.core.companygroups.data.repository.model;

import br.com.adtsys.ciee.model.system.core.base.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity(name = "empresa_grupos")
public class CompanyGroup extends AbstractEntity {

    private static final long serialVersionUID = 4943056929707566192L;

    @Id
    @GeneratedValue(generator = "custom-sequence", strategy = GenerationType.SEQUENCE)
    private Long id;


    @Column(name = "grupo_nome", length = 50, unique = true)
    @Size(message = "'Nome do Grupo' possui limite máximo de 50 caracteres", max = 50)
    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

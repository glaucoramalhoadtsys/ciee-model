package br.com.adtsys.ciee.model.system.auth.client.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Builder
@Data
public class SendEmailTemplateDTO {

    private List<String> toAddresses;
    private String templateIdentifier;
    private Map<String, String> templateParams;
}

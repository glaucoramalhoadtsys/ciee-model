package br.com.adtsys.ciee.model.test.school;

import br.com.adtsys.ciee.model.system.unit.educationinstitution.model.EducationInstitution;
import br.com.adtsys.ciee.model.system.unit.educationinstitution.model.EducationInstitutionDTO;
import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import br.com.adtsys.ciee.model.test.school.prerequest.SchoolPreAccessRequest;

import java.util.ArrayList;
import java.util.List;

public class SchoolTestObjects {

    private SchoolPreAccessRequest schoolPreAccessRequest;
    private EducationInstitution educationInstitution;
    private EducationInstitutionDTO educationInstitutionDTO;
    private List<Person> contacts;
    private Person director;
    private Person vicedirector;

    public SchoolTestObjects(SchoolPreAccessRequest schoolPreAccessRequest, EducationInstitution educationInstitution) {
        this.schoolPreAccessRequest = schoolPreAccessRequest;
        this.educationInstitution = educationInstitution;
        contacts = new ArrayList<Person>();
    }

    public SchoolTestObjects() {
    }

    public SchoolPreAccessRequest getSchoolPreAccessRequest() {
        return schoolPreAccessRequest;
    }

    public void setSchoolPreAccessRequest(SchoolPreAccessRequest schoolPreAccessRequest) {
        this.schoolPreAccessRequest = schoolPreAccessRequest;
    }

    public EducationInstitution getEducationInstitution() {
        return educationInstitution;
    }

    public void setEducationInstitution(EducationInstitution educationInstitution) {
        this.educationInstitution = educationInstitution;
    }

    public List<Person> getContacts() {
        return contacts;
    }

    public void setContacts(List<Person> contacts) {
        this.contacts = contacts;
    }

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    public Person getVicedirector() {
        return vicedirector;
    }

    public void setVicedirector(Person vicedirector) {
        this.vicedirector = vicedirector;
    }

    public EducationInstitutionDTO getEducationInstitutionDTO() {
        return educationInstitutionDTO;
    }

    public void setEducationInstitutionDTO(EducationInstitutionDTO educationInstitutionDTO) {
        this.educationInstitutionDTO = educationInstitutionDTO;
    }

    public void addNewContact(Person person){
        this.contacts.add(person);
    }
}

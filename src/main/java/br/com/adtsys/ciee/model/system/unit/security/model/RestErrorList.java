package br.com.adtsys.ciee.model.system.unit.security.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;

import static java.util.Arrays.asList;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class RestErrorList extends ArrayList<ErrorMessage> {


    private HttpStatus status;

    public RestErrorList(final HttpStatus status, final ErrorMessage... errors) {
        this(status.value(), errors);
    }

    public RestErrorList(final int status, final ErrorMessage... errors) {
        super();
        this.status = HttpStatus.valueOf(status);
        addAll(asList(errors));
    }

}

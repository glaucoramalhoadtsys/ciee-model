package br.com.adtsys.ciee.model.system.unit.educationinstitution.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EducationInstitutionPatchDTO {
    public String email;
    public Boolean ead;
    public Long walletId;
    private String cnpj;
    private String institutionName;
    private String shortInstitutionName;
    private String tradingName;
    private String shortTradingName;
    private String popularName;
    private String initials;
    private String stateRegistration;
    private String cityRegistration;
    private String maintainer;
    private String shortMaintainer;
    private String site;
}

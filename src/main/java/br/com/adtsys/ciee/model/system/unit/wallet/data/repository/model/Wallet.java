package br.com.adtsys.ciee.model.system.unit.wallet.data.repository.model;

import br.com.adtsys.ciee.model.system.unit.person.data.repository.model.Person;
import br.com.adtsys.ciee.model.system.unit.servicepoint.cieeunit.data.repository.model.CieeUnit;

public class Wallet {

  public Wallet() {}

  private Long id;

  private Person assistant;

  private CieeUnit cieeUnit;

  private String reducedDescription;

  private String description;

  private Boolean active;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Person getAssistant() {
    return assistant;
  }

  public void setAssistant(Person assistant) {
    this.assistant = assistant;
  }

  public CieeUnit getCieeUnit() {
    return cieeUnit;
  }

  public void setCieeUnit(CieeUnit cieeUnit) {
    this.cieeUnit = cieeUnit;
  }

  public String getReducedDescription() {
    return reducedDescription;
  }

  public void setReducedDescription(String reducedDescription) {
    this.reducedDescription = reducedDescription;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }
}

package br.com.adtsys.ciee.model.system.student.nationality.model;

import br.com.adtsys.ciee.model.system.student.nationality.brazilian.model.BrazilianInformation;
import br.com.adtsys.ciee.model.system.student.nationality.foreign.model.ForeignInformation;

public class NationalityInformation {

    private Long id;

    private Long nationalityId;

    private Boolean brazilian;

    private BrazilianInformation brazilianInformation;

    private ForeignInformation foreignInformation;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNationalityId() {
        return nationalityId;
    }

    public void setNationalityId(Long nationalityId) {
        this.nationalityId = nationalityId;
    }

    public Boolean getBrazilian() {
        return brazilian;
    }

    public void setBrazilian(Boolean brazilian) {
        this.brazilian = brazilian;
    }

    public BrazilianInformation getBrazilianInformation() {
        return brazilianInformation;
    }

    public void setBrazilianInformation(BrazilianInformation brazilianInformation) {
        this.brazilianInformation = brazilianInformation;
    }

    public ForeignInformation getForeignInformation() {
        return foreignInformation;
    }

    public void setForeignInformation(ForeignInformation foreignInformation) {
        this.foreignInformation = foreignInformation;
    }
}

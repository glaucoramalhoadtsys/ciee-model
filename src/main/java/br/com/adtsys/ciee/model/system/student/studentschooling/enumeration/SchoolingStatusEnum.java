package br.com.adtsys.ciee.model.system.student.studentschooling.enumeration;

/**
 * Situação da escolaridade cadastrada pelo estudante
 */
public enum SchoolingStatusEnum {

    PROGRESS("CURSANDO"),
    COMPLETED("CONCLUIDO"),
    INTERRUPTED("INTERROMPIDO");

    private String status;

    private SchoolingStatusEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}

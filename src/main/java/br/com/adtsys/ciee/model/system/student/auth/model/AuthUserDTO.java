package br.com.adtsys.ciee.model.system.student.auth.model;

import java.io.Serializable;

public class AuthUserDTO implements Serializable {


    private Long id;
    private String name;
    private String email;
    private String code;
    private String password;
    private String userType;
    private String roleName;
    private String cpf;
    private LoginDTO login;

}
